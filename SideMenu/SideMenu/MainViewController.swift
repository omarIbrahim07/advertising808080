//
//  MainViewController.swift
//  SideMenu
//
//  Created by Omar Ibrahim on 3/19/19.
//  Copyright © 2019 sideMenu. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onMenuTapped(_ sender: Any) {
        print("Menu Tapped")
        NotificationCenter.default.post(name: NSNotification.Name("ToogleSideMenu"), object: nil)
    }
    

}
