//
//  ViewController.swift
//  SideMenu
//
//  Created by Omar Ibrahim on 3/19/19.
//  Copyright © 2019 sideMenu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToogleSideMenu"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func toggleSideMenu() {
        if sideMenuOpen == true {
            sideMenuOpen = false
            sideMenuConstraint.constant = -240
        } else {
            sideMenuOpen = true
            sideMenuConstraint.constant = 0
        }
    }


}

