//
//  User.swift
//  Blabber
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    
    var userID : Int?
    var countryCode : String?
    var languageCode : String?
    var name : String?
    var photo : String?
    var about : String?
    var phone : String?
    var email : String?
    var lastLogInAt : String?
    var country : String?
    

    
    required override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userID <- map["id"]
        countryCode <- map["country_code"]
        languageCode <- map["language_code"]
        name <- map["name"]
        photo <- map["photo"]
        about <- map["about"]
        phone <- map["phone"]
        email <- map["email"]
        lastLogInAt <- map["created_at_ta"]
        country <- map["country"]
    }
    
    //MARK: - NSCoding -
    required init(coder aDecoder: NSCoder) {
        self.userID = aDecoder.decodeObject(forKey: "id") as? Int
        self.countryCode = aDecoder.decodeObject(forKey: "country_code") as? String
        self.languageCode = aDecoder.decodeObject(forKey: "language_code") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.photo = aDecoder.decodeObject(forKey: "photo") as? String
        self.about = aDecoder.decodeObject(forKey: "about") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.lastLogInAt = aDecoder.decodeObject(forKey: "created_at_ta") as? String
        self.country = aDecoder.decodeObject(forKey: "country") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userID, forKey: "id")
        aCoder.encode(countryCode, forKey: "country_code")
        aCoder.encode(languageCode, forKey: "language_code")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(photo, forKey: "photo")
        aCoder.encode(about, forKey: "about")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(lastLogInAt, forKey: "created_at_ta")
        aCoder.encode(country, forKey: "country")
    }
    
}
