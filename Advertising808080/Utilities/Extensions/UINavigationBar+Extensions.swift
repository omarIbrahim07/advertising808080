//
//  UINavigationBar+Extensions.swift
//  GameOn
//
//  Created by Hassan on 2/3/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func adjustDefaultNavigationBar() {
        self.barTintColor = mainBlueColor
        
        self.setBackgroundImage(UIImage(), for: .default)
        self.backgroundColor = mainBlueColor
        
        self.tintColor = UIColor.white

        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        
        self.isTranslucent = false
        self.shadowImage = UIImage()
    }
}
