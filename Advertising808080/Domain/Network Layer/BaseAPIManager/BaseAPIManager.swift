//
//  BaseAPIManager.swift
//  GameOn
//
//  Created by Hassan on 12/16/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation
import Alamofire

class BaseAPIManager : NSObject {
    
    typealias SuccessCompletion = (Any) -> Void
    
    typealias FailureCompletion = (APIError) -> Void
    
    //MARK:- JSON
    //Perform network request with JSON body in Post APIs
    func performNetworkRequest(forRouter router: BaseRouter , onSuccess: @escaping SuccessCompletion , onFailure: @escaping FailureCompletion) {
        
        print(self.JSONStringify(value: router.parameters as AnyObject, prettyPrinted: true))  // API parameters
        
        router.requestHeaders.updateValue("application/json", forKey: "Content-Type")
        
        Alamofire.request(router)
            .validate()
            .responseJSON { (response) in
                print(response.debugDescription)
                
                switch response.result {
                    
                case .success( _):
                    if let obj : [String : Any] = response.result.value as? [String : Any] {
                        return onSuccess(obj)
                    }
                    else {
                        let apiError = APIError()
                        return onFailure(apiError)
                    }
                    
                    
                case .failure(_):
                    let apiError = APIError()
                    apiError.error = response.result.error
                    apiError.responseStatusCode = response.response?.statusCode
                    
                    if let error = response.result.error as? AFError {
                        apiError.responseStatusCode = error._code // statusCode private
                    }
                    
                    return onFailure(apiError)
                    
                }
        }
    }
    
    //MARK:- Upload file
    func performUploadNetworkRequest(imageDataArray imgDataArray: [Data] ,forRouter router: BaseRouter , onSuccess: @escaping SuccessCompletion , onFailure: @escaping FailureCompletion)
    {
        print(self.JSONStringify(value: router.parameters as AnyObject, prettyPrinted: true))  // API parameters
                
        let multipartEncoding: (MultipartFormData) -> Void = { multipartFormData in
            
            var index = 0
            for imgData in imgDataArray {
                let imageFormat = ImageFormat.get(from: imgData)
                multipartFormData.append(imgData, withName: "pictures[]", fileName: "image\(index).\(imageFormat.rawValue)", mimeType: imageFormat.contentType)
                index += 1
            }
            
            for obj in router.parameters! {
                var string = obj.value as? String
                if obj.value is Int {
                    string = String(obj.value as! Int)
                }
                multipartFormData.append(string!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: obj.key)
            }
            
        }
        
        Alamofire.upload(
            multipartFormData: multipartEncoding,
            with: router,
            encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print(response.debugDescription)
                        if let error = response.result.error {
                            // Network request failed :(
                            let apiError = APIError()
                            apiError.error = error
                            return onFailure(apiError)
                        }
                        if let obj : [String : Any] = response.result.value as? [String : Any] {
                            return onSuccess(obj)
                        }
                    }
                    
                case .failure(let encodingError):
                    let apiError = APIError()
                    apiError.error = encodingError
                    
                    if let error = encodingError as? AFError {
                        apiError.responseStatusCode = error._code // statusCode private
                    }
                    
                    return onFailure(apiError)
                }
        })
    }
    
    //MARK:- Form-data
    //Perform network request with form-data body in Post APIs
    func performNetworkRequestWithFormData(forRouter router: BaseRouter , onSuccess: @escaping SuccessCompletion , onFailure: @escaping FailureCompletion) {
        
        print(self.JSONStringify(value: router.parameters as AnyObject, prettyPrinted: true))  // API parameters
        
        router.requestHeaders.updateValue("multipart/form-data", forKey: "Content-Type")

        let multipartEncoding: (MultipartFormData) -> Void = { multipartFormData in
            if let parameters = router.parameters {
                for obj in parameters {
                    var valueString: String = ""
                    
                    if let value: String = obj.value as? String {
                        valueString = value
                    }
                    else if let value: Int = obj.value as? Int {
                        valueString = String(value)
                    }
                    else if let value: Array<Any> = obj.value as? Array<Any> {
                        for item in value {
                            if let itemString: String = item as? String {
                                if let data = itemString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                                    multipartFormData.append(data, withName: obj.key)
                                }
                            }
                        }
                    }
                    
                    
                    if valueString.count > 0, let data = valueString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                        multipartFormData.append(data, withName: obj.key)
                    }
                    
                }
            }
        }
        
        Alamofire.upload(
            multipartFormData: multipartEncoding,
            with: router,
            encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print(response.debugDescription)
                        if let error = response.result.error {
                            // Network request failed :(
                            let apiError = APIError()
                            apiError.error = error
                            return onFailure(apiError)
                        }
                        else if let responseValue = response.result.value {
                            return onSuccess(responseValue)
                        }
                        else {
                            let apiError = APIError()
                            return onFailure(apiError)
                        }
                    }
                    
                case .failure(let encodingError):
                    let apiError = APIError()
                    apiError.error = encodingError
                    
                    if let error = encodingError as? AFError {
                        apiError.responseStatusCode = error._code // statusCode private
                    }
                    
                    return onFailure(apiError)
                }
        })
    }


    // MARK:- Helper
    func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }
            catch {
                print("error")
            }
            
        }
        return ""
    }

}
