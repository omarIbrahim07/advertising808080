//
//  Advertisement API Manager.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/10/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import Foundation
import ObjectMapper

class AdvertisementsAPIManager: BaseAPIManager {
    
    var parentID: String?
    
    func firstLevelPostAdvertisement(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: FIRST_LEVEL_POST_ADVERTISEMENT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let postID = response["Post ID"] as? Int {
                onSuccess(postID)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func editFirstLevelPostAdvertisement(postId: Int,basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let edit_FIRST_LEVEL_POST_ADVERTISEMENT_URL = "\(EDIT_FIRST_LEVEL_POST_ADVERTISEMENT_URL)\(postId)"
        
        let engagementRouter = BaseRouter(method: .post, path: edit_FIRST_LEVEL_POST_ADVERTISEMENT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let postID = response["Post ID"] as? Int {
                onSuccess(postID)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func saveAdvertisement(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SAVE_POST_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Int = response["status"] as? Int {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func sellAdvertisement(advertisementId: Int,basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let sell_Advertisement_URL = "\(SELL_ADVERTISEMENT_URL)\(advertisementId)"
        
        let engagementRouter = BaseRouter(method: .post, path: sell_Advertisement_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Int = response["Status"] as? Int {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func checkSaveAdvertisement(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: CHECK_SAVED_POST_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Int = response["status"] as? Int {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func deleteAdvertisement(id: Int, basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let delete_Post_Url = "\(DELETE_POST_URL)\(id)"
        
        let engagementRouter = BaseRouter(method: .post, path: delete_Post_Url, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: String = response["response"] as? String {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func saveAdPictures(imageDataArray: [Data], basicDictionary params:APIParams , onSuccess: @escaping ()->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SAVE_AD_PICS, parameters: params)

        super.performUploadNetworkRequest(imageDataArray: imageDataArray, forRouter: engagementRouter, onSuccess: { (response) in
            onSuccess()
        }) { (error) in
            onFailure(error)
        }
    }
    
    
}
