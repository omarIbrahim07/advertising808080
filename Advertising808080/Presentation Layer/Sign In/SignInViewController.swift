//
//  SignInViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/19/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {

    //Mark:- Outlets
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googlePlusButton: UIButton!
    @IBOutlet weak var guestsSignInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        configureUI()
        // Do any additional setup after loading the view.
    }
    
    func configureTextFields() {
        configureTextField(textField: emailAddressTextField, imageName: "icons8-new-post-50")
        configureTextField(textField: passwordTextField, imageName: "icons8-forgot-password-50")
    }
    
    func configureTextField(textField: UITextField, imageName: String) {
        textField.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let textFieldImage = UIImage(named: imageName)
        imageView.image = textFieldImage
        textField.rightView = imageView
    }
    
    func configureUI() {
        logInButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), borderWidth: 2)
        facebookButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        googlePlusButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        guestsSignInButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), borderWidth: 2)
    }
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        loginUser()
    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        print("Forgot Password")
    }
    
    
    
    @IBAction func haventRegistrationYetButtonPressed(_ sender: Any) {
        presentSignUp()
        print("Register is pressed")
    }
    
    
    func loginUser() {
        
        guard let email = emailAddressTextField.text, email.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        let parameters = [
            "email" : email as AnyObject,
            "password" : password as AnyObject,
            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AuthenticationAPIManager().loginUser(basicDictionary: parameters, onSuccess: { (_) in
            
            weakSelf?.getUserProfile()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func presentSignUp() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController")
        let naviagationController = UINavigationController(rootViewController: viewController)
        appDelegate.window!.rootViewController = naviagationController
        appDelegate.window!.makeKeyAndVisible()
    }

}
