//
//  SignUpViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/18/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class SignUpViewController: BaseViewController {

    private var fbData : [String : Any] = [:]
    
    var countries: [Country] = []
    var countryCodes: [String] = []
    var countryNames: [String] = []
    
    var countryCode: String?

    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var chooseCountryTextField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googlePlusButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        configureUI()
        createPicker()
        getCountries()
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().delegate = self
    }
    
    func configureTextFields() {
        configureTextField(textField: fullNameTextField, imageName: "icons8-user-50")
        configureTextField(textField: emailAddressTextField, imageName: "icons8-new-post-50")
        configureTextField(textField: phoneNumberTextField, imageName: "phoneIcon")
        configureTextField(textField: passwordTextField, imageName: "icons8-forgot-password-50")
        configureTextField(textField: confirmPasswordTextField, imageName: "icons8-forgot-password-50")
    }
    
    func configureTextField(textField: UITextField, imageName: String) {
        textField.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let textFieldImage = UIImage(named: imageName)
        imageView.image = textFieldImage
        textField.rightView = imageView
    }
    
    func configureUI() {
        registrationButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), borderWidth: 2)
        facebookButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        googlePlusButton.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func createPicker() {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        chooseCountryTextField.inputView = Picker
    }
    
    //MARK:- API Calls
    func getCountries() {
            
            self.startLoading()
            
            CategoriesAPIManager().getCountries(basicDictionary: [:], onSuccess: { (countries) in
                self.countries = countries
                
                for category in self.countries {
                    self.countryNames.append(category.name)
                    self.countryCodes.append(category.code)
                }
                
                self.stopLoadingWithSuccess()
                
            }) { (error) in
                print("error")
            }
        
    }
    
    
    func registerUser() {
        
        guard let username = fullNameTextField.text, username.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال اسم المستخدم"
            showError(error: apiError)
            return
        }
        
        guard let email = emailAddressTextField.text, email.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let mobileNumber = phoneNumberTextField.text, mobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let confirmPassword = confirmPasswordTextField.text, confirmPassword.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard password == confirmPassword else {
            let apiError = APIError()
            apiError.message = "كلمة المرور غير متطابقة"
            showError(error: apiError)
            return
        }
        
        guard let countryCode = self.countryCode , countryCode.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء اختيار البلد"
            showError(error: apiError)
            return
        }
        
        
        
        let parameters = [
            "email" : email as AnyObject,
            "name" : username as AnyObject,
            "phone" : mobileNumber as AnyObject,
            "password" : password as AnyObject,
            "verified_email" : "1" as AnyObject,
            "verified_phone" : "1" as AnyObject,
            "country_code" : countryCode as AnyObject
        ]
        
        weak var weakSelf = self
        
        startLoading()
        AuthenticationAPIManager().registerUser(basicDictionary: parameters, onSuccess: { () in
            
            weakSelf?.loginUser(email: email, password: password)
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func loginUser(email: String, password: String) {
        let parameters = [
            "email" : email as AnyObject,
            "password" : password as AnyObject,
        ]
        
        weak var weakSelf = self
        AuthenticationAPIManager().loginUser(basicDictionary: parameters, onSuccess: { (_) in
            
            
            weakSelf?.getUserProfile()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self

        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in

            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()

        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    //MARK:- Actions
    @IBAction func hadAcoountPressed(_ sender: Any) {
        presentLogIn()
        print("Had account")
    }
    
    @IBAction func agreeOnCoditions(_ sender: Any) {
        print("Agree on conditions")
    }
    
    @IBAction func registerActionPressed(_ sender: Any) {
        registerUser()
    }
    
    func presentLogIn() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignInViewController")
        let naviagationController = UINavigationController(rootViewController: viewController)
        appDelegate.window!.rootViewController = naviagationController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        let loginManager = FBSDKLoginManager()
        let facebookReadPermissions = ["public_profile", "email"]
        
        loginManager.logOut()
        
        loginManager.logIn(withReadPermissions: facebookReadPermissions, from: self) { (loginResult, FBSDKError) in
            if let error = FBSDKError {
                print("Facebook Error: \(error.localizedDescription)")
            }
            else {
                if (loginResult?.isCancelled)! {
                    print("User cancelled login.")
                }
                else {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @IBAction func googleAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func getFBUserData(){
        weak var weakSelf = self
        
        if let _ = FBSDKAccessToken.current() {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                guard let res = result as? [String : Any] else {
                    return
                }
                print("Facebook Data: \(res)")
                weakSelf?.fbData = res
                weakSelf?.createUserWithFacebook()
            })
        }
    }

    func createUserWithFacebook() {
        let FBToken = FBSDKAccessToken.current().tokenString

        guard let email = fbData["email"] as? String else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني في حساب الفيسبوك الخاص بك ثم معاودة المحاولة."
            showError(error: apiError)
            return
        }
        
        guard let name = fbData["name"] as? String, let _ = fbData["id"] as? String else {
            showError(error: APIError())
            return
        }
        
//        let profilePicLink = "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"

//        guard let mobileNumber = fbData["email"], mobileNumber.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
        
        
        let parameters = [
            "email" : email as AnyObject,
            "name" : name as AnyObject,
//            "phone" : mobileNumber as AnyObject,
            "password" : FBToken as AnyObject,
            "verified_email" : "1" as AnyObject,
            "verified_phone" : "1" as AnyObject
        ]
        
        weak var weakSelf = self
        
        startLoading()
        AuthenticationAPIManager().registerUser(basicDictionary: parameters, onSuccess: { () in
            
            weakSelf?.loginUser(email: email, password: FBToken!)
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }

    }
    
    
    //MARK:- Deinit
    deinit {
        print("Memory to be released soon (\(String(describing: self)))")
    }
    
        
}

extension SignUpViewController: GIDSignInUIDelegate {
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    private func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
//        stopLoadingWithSuccess()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SignUpViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            
            //Signup with backend
            
            // ...
        }
    }
}

extension SignUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chooseCountryTextField.text = countryNames[row]
        self.countryCode = countryCodes[row]
        print("\(countryCode)")
        self.view.endEditing(true)
    }

}

