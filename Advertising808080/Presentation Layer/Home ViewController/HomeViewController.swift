//
//  HomeViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/11/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    //MARK:- Variables
    var mainCategories = [MainCategories]()

    var subcatID: String?
    var searching = false

    //MARK:- Outlets
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var mainCategoriesSearchBar: UISearchBar!
    @IBOutlet weak var advertisingImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var advertisementEstablishButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
//        menuButton.target = self.revealViewController()
//        menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
//        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
//        self.revealViewController()?.rearViewRevealWidth = 300
        revealViewController()
        customizeNavigationBar()
        configureUI()
        configureTableView()
        getMainBanner()
        getMainCategories()
        mainCategoriesSearchBar.delegate = self
    }
    
    //MARK:- Configuration UI
    func customizeNavigationBar() {
        let navBarColor = #colorLiteral(red: 0.8862745098, green: 0.3176470588, blue: 0.2549019608, alpha: 1)
        navigationController?.navigationBar.barTintColor = navBarColor
        //        let navBarTitle = "الرئيسية"
        //        navigationItem.title = navBarTitle
    }
    
    func configureUI() {
        advertisementEstablishButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        advertisementEstablishButton.addCornerRadius(raduis: advertisementEstablishButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        mainCategoriesSearchBar.addCornerRadius(raduis: 4, borderColor: #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1), borderWidth: 1)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MainCategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "MainCategoriesTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getMainCategories() {
        
        //            let parameters = [
        //                "parent" : "457" as AnyObject,
        //                "fbclid" : "IwAR0VXWln-KoGgtAU4162o4TxsvXvX46BUD426eOLNC8UA_uJ4xXvp0cGjXE" as AnyObject
        //            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        CategoriesAPIManager().getMainCategories(basicDictionary: [:], onSuccess: { (Categories) in
            self.mainCategories = Categories
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getMainBanner() {
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        CategoriesAPIManager().getMainBanner(basicDictionary: [:], onSuccess: { (imageString) in

            let imageURL = "http://new.808080group.com/storage/\(imageString)"
            
            self.advertisingImage.loadImageFromUrl(imageUrl: imageURL)
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }
    
    //MARK:- Segue to SubCategories
    func goToSubCategories(parent: String) {
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "SubCategoriesViewController") as? SubCategoriesViewController {
            viewController.categoryParentId = parent
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
    func goToSearchingAdvertisements(searchedStringKey: String) {
        if let searchingNavigationController = storyboard?.instantiateViewController(withIdentifier: "SearchingControllerNavigationController") as? UINavigationController, let rootViewContoller = searchingNavigationController.viewControllers[0] as? SearchingViewController {
            rootViewContoller.searchedString = searchedStringKey
            self.present(searchingNavigationController, animated: true, completion: nil)
        }
    }

    
    //MARK:- When Menu Tapped
    @IBAction func onMenuTapped(_ sender: Any) {
        print("Menu Tapped")
        NotificationCenter.default.post(name: NSNotification.Name("ToogleSideMenu"), object: nil)
    }
    
    //MARK:- When Add advertisement Button tapped
    @IBAction func addAdvertisementButtonPressed(_ sender: Any) {
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 0
            self.present(addNavigationController, animated: true, completion: nil)
        }
    }
    

}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: MainCategoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MainCategoriesTableViewCell") as? MainCategoriesTableViewCell {
            
            let mainCategoryType = mainCategories[indexPath.row]
            if mainCategoryType.image.count > 0 {
                let imageURL = "http://new.808080group.com/storage/"+String(mainCategories[indexPath.row].image)
                cell.mainCategoryImage.loadImageFromUrl(imageUrl: imageURL)
            }
            cell.mainCategoryTitle.text = mainCategoryType.name
//            cell.mainCategoryAdvertisementNumLabel.text = String(mainCategoryType.postsCount) + " إعلان"

            return cell
        }
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.revealViewController() != nil {
            self.revealViewController().revealToggle(animated: true)
        }
//        subcatID = mainCategories[indexPath.row].parent
        subcatID = String(mainCategories[indexPath.row].id)
        goToSubCategories(parent: subcatID!)
    }

}

 extension HomeViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            // here is text from the search bar
            print(text)
            
//            userInput = text
            // u must pass text here to searching viewcontroller ,, so u must make goToSerching take the text there
            goToSearchingAdvertisements(searchedStringKey: text)
        }
    }
}
