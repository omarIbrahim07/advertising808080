//
//  MainCategoriesTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/11/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class MainCategoriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainCategoryImage: UIImageView!
    @IBOutlet weak var mainCategoryTitle: UILabel!
    @IBOutlet weak var mainCategoryAdvertisementNumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
