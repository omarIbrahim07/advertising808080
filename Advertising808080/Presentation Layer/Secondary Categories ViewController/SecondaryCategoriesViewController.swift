//
//  CategoriesViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/12/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SecondaryCategoriesViewController: BaseViewController {
    
    //MARK:- Variables
    var secondaryCategories : [AdvertisementDetailsModel] = []
    var specialSecondaryCategories : [AdvertisementDetailsModel] = []

    var secondaryParentId: String?
    
    var advertisementID: String?

    //MARK:- Outlets
    @IBOutlet weak var advertisementSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAdvertisementButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "الإعلانات"
        configureUI()
        configureTableView()
        getSpecialSecondaryCategories()
        getSecondaryCategories()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Configuration UI
    func configureUI() {
        addAdvertisementButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        addAdvertisementButton.addCornerRadius(raduis: addAdvertisementButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "AdvertisementShowTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementShowTableViewCell")
        tableView.register(UINib(nibName: "SpecialAdvertisementsTableViewCell", bundle: nil), forCellReuseIdentifier: "SpecialAdvertisementsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getSecondaryCategories() {
        
        self.startLoading()
        
        CategoriesAPIManager().getSecondaryCategories(Parent: secondaryParentId!, basicDictionary: [:], onSuccess: { (secondaryCategories) in
            self.secondaryCategories = secondaryCategories
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getSpecialSecondaryCategories() {
        
        self.startLoading()
        
        CategoriesAPIManager().getSpecialSecondaryCategories(Parent: secondaryParentId!, basicDictionary: [:], onSuccess: { (specialSecondaryCategories) in
            
            self.specialSecondaryCategories = specialSecondaryCategories
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    

    //MARK:- Segue to Advertisement Details
    func goToAdvertisementDetails(parent: String) {
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementID = parent
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
    
    @IBAction func addAdvertisementButtonPressed(_ sender: Any) {
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 0
            self.present(addNavigationController, animated: true, completion: nil)
        }
    }
    

}

//MARK:- TableView
extension SecondaryCategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfCells = (secondaryCategories.count) + 1
        return numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
        if let cell: SpecialAdvertisementsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SpecialAdvertisementsTableViewCell") as? SpecialAdvertisementsTableViewCell {

            cell.viewModels = specialSecondaryCategories
            cell.delegate = self
            return cell
            }
        }
    
        else if indexPath.row > 0 {
            let newIndex = (indexPath.row) - 1
        if let cell: AdvertisementShowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementShowTableViewCell") as? AdvertisementShowTableViewCell {
            
            if let advertisementImage = self.secondaryCategories[newIndex].advertisementImage {
                let adImage = "http://new.808080group.com/storage/"+String(advertisementImage)
                print(adImage)
                cell.advertisementImage.loadImageFromUrl(imageUrl: adImage)
            }
            
            cell.advertisementName.text = "لا يوجد عنوان للإعلان"
            if let advertiseName = self.secondaryCategories[newIndex].title {
                cell.advertisementName.text = advertiseName
            }
            
            cell.advertisementPrice.text = "لا يوجد سعر للإعلان"
            if let advertisePrice = self.secondaryCategories[newIndex].price {
                cell.advertisementPrice.text = advertisePrice
            }
            
            cell.advertisementTime.text = "لا يوجد"
            if let advertiseTime = self.secondaryCategories[newIndex].createdAt {
                cell.advertisementTime.text = advertiseTime
            }
            
            cell.advertisementPlace.text = "لا يوجد مكان للإعلان"
            if let advertisePlace = self.secondaryCategories[newIndex].city {
                cell.advertisementPlace.text = advertisePlace
            }
            
            return cell
        }
    }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row > 0 {
            let newIndex = (indexPath.row) - 1
            advertisementID = String(secondaryCategories[newIndex].id)
            goToAdvertisementDetails(parent: advertisementID!)
        }
    }
    
}

//MARK:- SpecialAdvertisementsTableViewCellDelegate
extension SecondaryCategoriesViewController: SpecialAdvertisementsTableViewCellDelegate {
    
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
//        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
//            viewController.advertisementDetails = viewModel
//            viewController.advertisementID = "\(viewModel.id!)"
//            appDelegate.window!.rootViewController = viewController
//            appDelegate.window!.makeKeyAndVisible()
//        }
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
}
