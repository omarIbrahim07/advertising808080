//
//  AdvertisementShowTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/13/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class AdvertisementShowTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var advertisementImage: UIImageView!
    @IBOutlet weak var advertisementName: UILabel!
    @IBOutlet weak var advertisementPrice: UILabel!
    @IBOutlet weak var advertisementTime: UILabel!
    @IBOutlet weak var advertisementPlace: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        selectionStyle = .none
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
        advertisementImage.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
}
