//
//  SpecialAdvertisementsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/13/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol SpecialAdvertisementsTableViewCellDelegate {
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

class SpecialAdvertisementsTableViewCell: UITableViewCell {

    @IBOutlet weak var specialAdertisementCollectionView: UICollectionView!
    
    var parentVC: SecondaryCategoriesViewController?
    
    var delegate: SpecialAdvertisementsTableViewCellDelegate?
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.specialAdertisementCollectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionView()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        // Configure the view for the selected state
    }
    
    func configureCollectionView() {
        specialAdertisementCollectionView.register(UINib(nibName: "SpecialAdvertisementCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SpecialAdvertisementCollectionViewCell")
        specialAdertisementCollectionView.delegate = self
        specialAdertisementCollectionView.dataSource = self
        specialAdertisementCollectionView.reloadData()
    }
    
    //MARK:- Delegate Helpers
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectCollectionViewCell(viewModel: viewModel)
        }
    }
    
}

extension SpecialAdvertisementsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: SpecialAdvertisementCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialAdvertisementCollectionViewCell", for: indexPath) as? SpecialAdvertisementCollectionViewCell {
            
            cell.viewModell = viewModels[indexPath.row]
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
    
    
}
