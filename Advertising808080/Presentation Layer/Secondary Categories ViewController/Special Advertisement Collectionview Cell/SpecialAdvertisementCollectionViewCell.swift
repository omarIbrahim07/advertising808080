//
//  SpecialAdvertisementCollectionViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/13/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SpecialAdvertisementCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var specialAdvertisementImage: UIImageView!
    @IBOutlet weak var specialAdvertisementLabel: UILabel!
    @IBOutlet weak var specialAdvertisementNameLabel: UILabel!
    @IBOutlet weak var specialAdvertisementLocationLabel: UILabel!
    @IBOutlet weak var specialAdvertisementCostLabel: UILabel!
    
    var viewModell: AdvertisementDetailsModel! {
        didSet{
            bindData()
        }
    }
    
    var similarAdvertisementViewModel: AdvertisementDetailsModel! {
        didSet{
            similarAdvertisementsBindData()
        }
    }
    
    func similarAdvertisementsBindData() {
        
        specialAdvertisementNameLabel.text = similarAdvertisementViewModel.title
        specialAdvertisementCostLabel.text = similarAdvertisementViewModel.price
        specialAdvertisementLocationLabel.text = similarAdvertisementViewModel.address
        
        
        if let advertisementImage = similarAdvertisementViewModel.advertisementImage {
            let url: URL = URL(string: "http://new.808080group.com/storage/\(advertisementImage)")!
            self.specialAdvertisementImage.kf.setImage(with: url)
        }
        
    }
    
    func bindData() {
        
        specialAdvertisementNameLabel.text = "لا يوجد عنوان للإعلان"
        
        if let title = viewModell.title {
            specialAdvertisementNameLabel.text = title
        }
        
        specialAdvertisementLocationLabel.text = "لا يوجد مكان"
        
        if let place = viewModell.city {
            specialAdvertisementLocationLabel.text = place
        }
        
        specialAdvertisementCostLabel.text = "لا يوجد سعر للإعلان"
        
        if let price = viewModell.price {
            specialAdvertisementCostLabel.text = price
        }
        
        if let advertisementImage = viewModell.advertisementImage {
            let url: URL = URL(string: "http://new.808080group.com/storage/\(advertisementImage)")!
            self.specialAdvertisementImage.kf.setImage(with: url)
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
        specialAdvertisementLabel.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 1)
//        specialAdvertisementImage.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }


}
