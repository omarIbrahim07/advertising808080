//
//  MyAdvertisementCollectionViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/24/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol MyAdvertisementCollectionViewCellDeleteButtonDelegate {
    func didDeleteButtonPressed(id: Int)
}

protocol MyAdvertisementCollectionViewCellSoldButtonDelegate {
    func didSoldButtonPressed(id: Int)
}

protocol MyAdvertisementCollectionViewCellEditButtonDelegate {
    func didEditButtonPressed(postId: Int)
}

class MyAdvertisementCollectionViewCell: UICollectionViewCell {
    
    var Selected: String?
    
    var delegate: MyAdvertisementCollectionViewCellDeleteButtonDelegate?
    var soldDelegate: MyAdvertisementCollectionViewCellSoldButtonDelegate?
    var editDelegate: MyAdvertisementCollectionViewCellEditButtonDelegate?

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var advertisementImage: UIImageView!
    @IBOutlet weak var advertisementNameLabel: UILabel!
    @IBOutlet weak var advertisementPlaceLabel: UILabel!
    @IBOutlet weak var advertisementPriceLabel: UILabel!
    @IBOutlet weak var advertisementStateLabel: UILabel!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var sellButton: UIButton!
    
    var viewModell: AdvertisementDetailsModel! {
        didSet{
            bindData()
        }
    }
    
    func bindData() {
        
        advertisementNameLabel.text = "لا يوجد عنوان للإعلان"
        
        if let title = viewModell.title {
            advertisementNameLabel.text = title
        }
        
        advertisementPlaceLabel.text = "لا يوجد مكان"
        if let place = viewModell.city {
            advertisementPlaceLabel.text = place
        }
        
        advertisementStateLabel.text = "لا يوجد"
//        if let state = viewModell.state {
//            advertisementStateLabel.text = state
//        }
        
        advertisementPriceLabel.text = "لا يوجد سعر للإعلان"
        
        if let price = viewModell.price {
            advertisementPriceLabel.text = price
        }
        
        if let advertisementImage = viewModell.advertisementImage {
            let url: URL = URL(string: "http://new.808080group.com/storage/\(advertisementImage)")!
            self.advertisementImage.kf.setImage(with: url)
            print("7obby")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
        deleteView.addCornerRadius(raduis: 0, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
        editView.addCornerRadius(raduis: 0, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didDeleteButtonPressed(id: Int) {
        if let delegateValue = delegate {
            delegateValue.didDeleteButtonPressed(id: id)
        }
    }
    
    //MARK:- Delegate Helpers
    func didSoldButtonPressed(id: Int) {
        if let delegateValue = soldDelegate {
            delegateValue.didSoldButtonPressed(id: id)
        }
    }
    
    //MARK:- Delegate Helpers
    func didEditButtonPressed(postId: Int) {
        if let delegateValue = editDelegate {
            delegateValue.didEditButtonPressed(postId: postId)
        }
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        print("Edit Button Pressed")
        didEditButtonPressed(postId: viewModell.id)
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        print("Delete is pressed")
        didDeleteButtonPressed(id: viewModell.id)
    }
    
    @IBAction func soldButtonPressed(_ sender: Any) {
        print("Sold Button is pressed")
        didSoldButtonPressed(id: viewModell.id)
    }
    
}

