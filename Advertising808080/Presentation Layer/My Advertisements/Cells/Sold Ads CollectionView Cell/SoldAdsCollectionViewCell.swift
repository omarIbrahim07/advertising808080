//
//  SoldAdsCollectionViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/24/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol SoldAdCollectionViewCellDeleteButtonDelegate {
    func didSoldAdDeleteButtonPressed(id: Int)
}

class SoldAdsCollectionViewCell: UICollectionViewCell {
    
    var delegate: SoldAdCollectionViewCellDeleteButtonDelegate?

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var soldButton: UIButton!
    @IBOutlet weak var advertisementImage: UIImageView!
    @IBOutlet weak var advertisementNameLabel: UILabel!
    @IBOutlet weak var advertisementPriceLabel: UILabel!
    @IBOutlet weak var advertisementPlaceLabel: UILabel!
    
    var viewModell: AdvertisementDetailsModel! {
        didSet{
            bindData()
        }
    }
    
    func bindData() {
        
        advertisementNameLabel.text = "لا يوجد عنوان للإعلان"
        
        if let title = viewModell.title {
            advertisementNameLabel.text = title
        }
        
        advertisementPlaceLabel.text = "لا يوجد مكان"
        if let place = viewModell.city {
            advertisementPlaceLabel.text = place
        }
        
        advertisementPriceLabel.text = "لا يوجد سعر للإعلان"
        
        if let price = viewModell.price {
            advertisementPriceLabel.text = price
        }
        
        if let advertisementImage = viewModell.advertisementImage {
            let url: URL = URL(string: "http://new.808080group.com/storage/\(advertisementImage)")!
            self.advertisementImage.kf.setImage(with: url)
            print("7obby")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didSoldAdDeleteButtonPressed(id: Int) {
        if let delegateValue = delegate {
            delegateValue.didSoldAdDeleteButtonPressed(id: id)
        }
    }
    
    @IBAction func deleteButtonIsPressed(_ sender: Any) {
        print("deleted is pressed")
        didSoldAdDeleteButtonPressed(id: viewModell.id)
    }
    
    @IBAction func soldButtonIsPressed(_ sender: Any) {
        print("Sold is pressed")
    }
    
}
