//
//  OptionsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var optionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
