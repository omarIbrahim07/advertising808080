//
//  OtherAdsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol OtherAdsTableViewCellDelegate {
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

protocol ReloadSavedAdsTableViewCellDelegate {
    func didRemoveSavedAdvertisement()
}

//protocol ReloadSoldRemovedAdsTableViewCellDelegate {
//    func didRemoveSoldAdvertisement()
//}

class OtherAdsTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: OtherAdsTableViewCellDelegate?
    var reloadDelegate: ReloadSavedAdsTableViewCellDelegate?
//    var reloadRemovedSoldDelegate: ReloadSoldRemovedAdsTableViewCellDelegate?
    
    var unsavedAdvertisementId: Int?
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionViewCell()
        // Initialization code
    }
    
    
    func configureCollectionViewCell() {
        collectionView.register(UINib(nibName: "OtherAdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OtherAdsCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func unsavePost() {
        
        guard let postID = self.unsavedAdvertisementId, postID > 0 else {
            let apiError = APIError()
            apiError.message = "غير قادر"
            showError(error: apiError)
            return
        }
        
        print("postID: \(postID)")
        
        
        let parameters = [
            "postId" : postID as AnyObject,
            ]
        
        AdvertisementsAPIManager().saveAdvertisement(basicDictionary: parameters, onSuccess: { (message) in
            print(message)
            
            if message == 1 {
                print("تم الحفظ")
            } else if message == 0 {
                print("تم إلغاء الحفظ")
            }
            self.didRemoveSavedAdvertisement()
            
        }) { (error) in
            
        }
        
    }
    
    //MARK:- Delegate Helpers
    func didRemoveSavedAdvertisement() {
        if let delegateValue = reloadDelegate {
            delegateValue.didRemoveSavedAdvertisement()
        }
    }
    
//    //MARK:- Delegate Helpers
//    func didRemoveSoldAdvertisement() {
//        if let delegateValue = reloadRemovedSoldDelegate {
//            delegateValue.didRemoveSoldAdvertisement()
//        }
//    }
    
    //MARK:- Delegate Helpers
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectCollectionViewCell(viewModel: viewModel)
        }
    }
    
}



extension OtherAdsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: OtherAdsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherAdsCollectionViewCell", for: indexPath) as? OtherAdsCollectionViewCell {
            cell.delegate = self
            cell.viewModell = viewModels[indexPath.row]
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
}

extension OtherAdsTableViewCell: OtherAdsCollectionViewCellUnsaveButtonDelegate {
    
    func didUnsaveButtonPressed(id: Int) {
        self.unsavedAdvertisementId = id
        unsavePost()
    }
    
    
}

//extension OtherAdsTableViewCell: OtherAdsCollectionViewCellDeleteButtonDelegate {

//    func didOtherCollectionViewCellDeleteButtonPressed(id: Int) {
//        <#code#>
//    }
    

//    func didUnsaveButtonPressed(id: Int) {
//        self.unsavedAdvertisementId = id
//        unsavePost()
//    }


//}
