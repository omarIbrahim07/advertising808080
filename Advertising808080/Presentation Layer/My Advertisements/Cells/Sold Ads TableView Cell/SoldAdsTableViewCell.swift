//
//  SoldAdsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/24/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol SoldAdsTableViewCellDelegate {
    func didSelectSoldAdCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

protocol ReloadAfterDeleteSoldAdTableViewCellDelegate {
    func didRemoveSoldAdvertisement()
}

class SoldAdsTableViewCell: UITableViewCell {
    
    var deletedAdvertisementId: Int?
    
    var delegate: SoldAdsTableViewCellDelegate?
    
    var deletionDelegate: ReloadAfterDeleteSoldAdTableViewCellDelegate?
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionViewCell()
        // Initialization code
    }
    
    func configureCollectionViewCell() {
        collectionView.register(UINib(nibName: "SoldAdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SoldAdsCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func deletePost() {
        
        guard let postID = self.deletedAdvertisementId, postID > 0 else {
            let apiError = APIError()
            apiError.message = "غير قادر"
            showError(error: apiError)
            return
        }
        
        print("postID: \(postID)")
        
        AdvertisementsAPIManager().deleteAdvertisement(id: postID ,basicDictionary: [:], onSuccess: { (message) in
            print(message)
            
            if message == "Ad deleted Successfully" {
                print("تم المسح")
            }
            
            self.didRemoveSoldAdvertisement()
            
        }) { (error) in
            
        }
        
    }
    
    //MARK:- Delegate Helpers
    func didSelectSoldAdCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectSoldAdCollectionViewCell(viewModel: viewModel)
        }
    }
    
    //MARK:- Delegate Helpers
    func didRemoveSoldAdvertisement() {
        if let delegateValue = deletionDelegate {
            delegateValue.didRemoveSoldAdvertisement()
        }
    }
    
}

extension SoldAdsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: SoldAdsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SoldAdsCollectionViewCell", for: indexPath) as? SoldAdsCollectionViewCell {
            cell.delegate = self
            cell.viewModell = viewModels[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectSoldAdCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
    
}

extension SoldAdsTableViewCell: SoldAdCollectionViewCellDeleteButtonDelegate {
    
    func didSoldAdDeleteButtonPressed(id: Int) {
        self.deletedAdvertisementId = id
        deletePost()
    }
}
