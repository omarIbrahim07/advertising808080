//
//  OtherAdsCollectionViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol OtherAdsCollectionViewCellUnsaveButtonDelegate {
    func didUnsaveButtonPressed(id: Int)
}

//protocol OtherAdsCollectionViewCellDeleteButtonDelegate {
//    func didOtherCollectionViewCellDeleteButtonPressed(id: Int)
//}

class OtherAdsCollectionViewCell: UICollectionViewCell {
    
    var delegate: OtherAdsCollectionViewCellUnsaveButtonDelegate?

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var advertisementStateLabel: UILabel!
    @IBOutlet weak var advertisementImage: UIImageView!
    @IBOutlet weak var advertisementNameLabel: UILabel!
    @IBOutlet weak var advertisementPriceLabel: UILabel!
    @IBOutlet weak var advertisementPlaceLabel: UILabel!
    
    var viewModell: AdvertisementDetailsModel! {
        didSet{
            bindData()
        }
    }
    
    func bindData() {
        
        advertisementNameLabel.text = "لا يوجد عنوان"
        if let advertisementName = viewModell.title {
            advertisementNameLabel.text = advertisementName
        }
        
        advertisementPriceLabel.text = "لا يوجد سعر"
        if let advertisementPrice = viewModell.price {
            advertisementPriceLabel.text = advertisementPrice
        }
        
        advertisementPlaceLabel.text = "لا يوجد مكان"
        if let advertisementPlace = viewModell.city {
            advertisementPlaceLabel.text = advertisementPlace
        }
        
        if let advertisementImage = viewModell.advertisementImage {
            let url: URL = URL(string: "http://new.808080group.com/storage/\(advertisementImage)")!
            self.advertisementImage.kf.setImage(with: url)
            print("7obby")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didUnsaveButtonPressed(id: Int) {
        if let delegateValue = delegate {
            delegateValue.didUnsaveButtonPressed(id: id)
        }
    }
    
//    @IBAction func onDeletePressed(_ sender: Any) {
//        print("Unsaved")
//        didOtherCollectionViewCellDeleteButtonPressed(id: viewModell.id)
////        didUnsaveButtonPressed(id: viewModell.id)
//    }
    
}
