//
//  MyAdvertisementTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/24/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol MyAdvertisementTableViewCellDelegate {
    func didSelectOtherCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

protocol ReloadAfterDeleteAdTableViewCellDelegate {
    func didRemoveAdvertisement()
}

protocol ReloadAfterSellAdTableViewCellDelegate {
    func didSellAdvertisement()
}

protocol GoToEditAdTableViewCellDelegate {
    func didEditAdvertisement(postId: Int)
}

class MyAdvertisementTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var parentVC: AdvertisementsViewController?
    
    var deletedAdvertisementId: Int?
    var soldAdvertisementId : Int?
    var editAdvertisementId : Int?
    
    var delegate: MyAdvertisementTableViewCellDelegate?
    var deletionDelegate: ReloadAfterDeleteAdTableViewCellDelegate?
    var soldDelegate: ReloadAfterSellAdTableViewCellDelegate?
    var editDelegate: GoToEditAdTableViewCellDelegate?
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.collectionView.reloadData()
            print("دخلت")
        }
    }
    
    var Selected: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionViewCell()
        // Initialization code
    }
    
    func configureCollectionViewCell() {
        collectionView.register(UINib(nibName: "MyAdvertisementCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyAdvertisementCollectionViewCell")
        collectionView.register(UINib(nibName: "OtherAdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OtherAdsCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func deletePost() {
        
        guard let postID = self.deletedAdvertisementId, postID > 0 else {
            let apiError = APIError()
            apiError.message = "غير قادر"
            showError(error: apiError)
            return
        }
        
        print("postID: \(postID)")
        
        AdvertisementsAPIManager().deleteAdvertisement(id: postID ,basicDictionary: [:], onSuccess: { (message) in
            print(message)
            
            if message == "Ad deleted Successfully" {
                print("تم المسح")
            }
            
            self.didRemoveAdvertisement()
            
        }) { (error) in
            
        }
        
    }
    
    func sellAdvertisement() {
        
        guard let postID = self.soldAdvertisementId, postID > 0 else {
            let apiError = APIError()
            apiError.message = "غير قادر"
            showError(error: apiError)
            return
        }
        
        print("postID: \(postID)")
        
        AdvertisementsAPIManager().sellAdvertisement(advertisementId: postID ,basicDictionary: [:], onSuccess: { (message) in
            print(message)
            
            if message == 1 {
                print("إتباع")
            } else if message == 0 {
                print("متباعش")
            }
            
            self.didSellAdvertisement()
            
        }) { (error) in
            
        }
        
    }
    
    //MARK:- Delegate Helpers
    func didSelectOtherCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectOtherCollectionViewCell(viewModel: viewModel)
        }
    }
    
    //MARK:- Delegate Helpers
    func didRemoveAdvertisement() {
        if let delegateValue = deletionDelegate {
            delegateValue.didRemoveAdvertisement()
        }
    }
    
    //MARK:- Delegate Helpers
    func didSellAdvertisement() {
        if let delegateValue = soldDelegate {
            delegateValue.didSellAdvertisement()
        }
    }
    
    //MARK:- Delegate Helpers
    func didEditAdvertisement(postId: Int) {
        if let delegateValue = editDelegate {
            delegateValue.didEditAdvertisement(postId: postId)
        }
    }
    
}


extension MyAdvertisementTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: MyAdvertisementCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyAdvertisementCollectionViewCell", for: indexPath) as? MyAdvertisementCollectionViewCell {
            
            if let isSaved = viewModels[indexPath.row].archived {
                if isSaved == "1" {
                    cell.sellButton.setImage(UIImage(named: "sold"), for: .normal)
                } else if isSaved == "0" {
                    cell.sellButton.setImage(UIImage(named: "uncheck"), for: .normal)
                }
            }
            
            cell.delegate = self
            cell.soldDelegate = self
            cell.editDelegate = self
            cell.viewModell = viewModels[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectOtherCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
    
}

extension MyAdvertisementTableViewCell: MyAdvertisementCollectionViewCellDeleteButtonDelegate {
    
    func didDeleteButtonPressed(id: Int) {
        self.deletedAdvertisementId = id
        deletePost()
    }
}

extension MyAdvertisementTableViewCell: MyAdvertisementCollectionViewCellSoldButtonDelegate {

    func didSoldButtonPressed(id: Int) {
        self.soldAdvertisementId = id
        sellAdvertisement()
    }
}

extension MyAdvertisementTableViewCell: MyAdvertisementCollectionViewCellEditButtonDelegate {
    
    func didEditButtonPressed(postId: Int) {
        self.editAdvertisementId = postId
        didEditAdvertisement(postId: self.editAdvertisementId!)
    }
}
