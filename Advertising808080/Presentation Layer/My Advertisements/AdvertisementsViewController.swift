//
//  AdvertisementsViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class AdvertisementsViewController: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAdvertisementButton: UIButton!
    
    //MARK:- Variables
    var selected = 0
    
    var allMyAdvertisements : [AdvertisementDetailsModel] = []
    var allMySpecialAdvertisements : [AdvertisementDetailsModel] = []
    var savedAdvertisements : [AdvertisementDetailsModel] = []
    var soldAdvertisements: [AdvertisementDetailsModel] = []
    
    var soldAdvertisementId : Int?
    var editAdvertisementId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureTableView()
        getAllMyAdvertisements()
        getSoldAdvertisements()
        getAllMySpecialAdvertisements()
        getSavedAdvertisements()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Configure UI
    func configureUI() {
        addAdvertisementButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        addAdvertisementButton.addCornerRadius(raduis: addAdvertisementButton.frame.height / 2 , borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "OptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "OptionsTableViewCell")
        tableView.register(UINib(nibName: "MyAdvertisementTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAdvertisementTableViewCell")
        tableView.register(UINib(nibName: "EditProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileTableViewCell")
        tableView.register(UINib(nibName: "OtherAdsTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherAdsTableViewCell")
          tableView.register(UINib(nibName: "SoldAdsTableViewCell", bundle: nil), forCellReuseIdentifier: "SoldAdsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getAllMyAdvertisements() {
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else {
            return
        }
        
        self.startLoading()
        
        CategoriesAPIManager().getAllMyAdvertisements(id: userID, basicDictionary: [:], onSuccess: { (allMyAdvertisements) in
            
            self.allMyAdvertisements = allMyAdvertisements
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getAllMySpecialAdvertisements() {
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else {
            return
        }
        
        self.startLoading()
        
        CategoriesAPIManager().getAllMySpecialAdvertisements(id: userID, basicDictionary: [:], onSuccess: { (allMySpecialAdvertisements) in
            
            self.allMySpecialAdvertisements = allMySpecialAdvertisements
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getSoldAdvertisements() {
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else {
            return
        }
        
        self.startLoading()
        
        CategoriesAPIManager().getSoldAdvertisements(id: userID, basicDictionary: [:], onSuccess: { (soldAdvertisements) in
            
            self.soldAdvertisements = soldAdvertisements
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getSavedAdvertisements() {
        
        guard let userID = UserDefaultManager.shared.currentUser?.userID else {
            return
        }
        
        self.startLoading()
        
        CategoriesAPIManager().getSavedAdvertisements(userID: userID, basicDictionary: [:], onSuccess: { (savedAdvertisements) in
            
            self.savedAdvertisements = savedAdvertisements
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }
    
    func sellAdvertisement() {
        
        guard let soldAdvertisementId = self.soldAdvertisementId else {
            return
        }
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AdvertisementsAPIManager().sellAdvertisement(advertisementId: soldAdvertisementId, basicDictionary: [:], onSuccess: { (message) in
            print(message)
            
            weakSelf?.stopLoadingWithSuccess()
            
//            if message == 1 {
//                print("تم الحفظ")
//                self.isSaved = message
//            } else if message == 0 {
//                print("تم إلغاء الحفظ")
//                self.isSaved = message
//            }
            
            self.tableView.reloadData()
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func goToEditAddAdvertisementLevelOne() {
        
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 1
            rootViewContoller.editedAdvertisementId = self.editAdvertisementId
            self.present(addNavigationController, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- Actions
    @IBAction func addClicked(_ sender: Any) {
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 0
            self.present(addNavigationController, animated: true, completion: nil)
        }
    }

}


extension AdvertisementsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 4
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            let headerText = UILabel()
            headerText.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            headerText.adjustsFontSizeToFitWidth = true


            headerText.textAlignment = .right
            headerText.text = "  إعلاناتي"
            return headerText
        }

        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
            return 30
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let cell: EditProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTableViewCell") as? EditProfileTableViewCell {
                
//                if let userImage = UserDefaultManager.shared.currentUser.image {
//                    let userImageUrl = "http://new.808080group.com/storage/"+String(userImage)
//                    print(userImage)
//                    cell.advertisementImage.loadImageFromUrl(imageUrl: userImageUrl)
//                }
                
                cell.userNameLabel.text = UserDefaultManager.shared.currentUser?.name
                cell.hoursLabel.text = UserDefaultManager.shared.currentUser?.lastLogInAt
                cell.starView.isHidden = true
                cell.starEvaluationLabel.isHidden = true
                
                return cell
            }
        }
        
        if indexPath.section == 1 {
            
            if indexPath.row == 0 {
                if let cell: OptionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell") as? OptionsTableViewCell {
                    cell.optionLabel.text = "كل إعلاناتي"
                    return cell
                }
            }
            
            else if indexPath.row == 1 {
                if let cell: OptionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell") as? OptionsTableViewCell {
                    cell.optionLabel.text = "إعلاناتي المباعة"
                    return cell
                }
            }
                
            else if indexPath.row == 2 {
                if let cell: OptionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell") as? OptionsTableViewCell {
                    cell.optionLabel.text = "إعلاناتي المميزة"
                    return cell
                }
            }
            
            else if indexPath.row == 3 {
                if let cell: OptionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell") as? OptionsTableViewCell {
                    cell.optionLabel.text = "إعلاناتي المفضلة"
                    return cell
                }
            }

        }
        
        else {
            if selected == 0 {
                if let cell: MyAdvertisementTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyAdvertisementTableViewCell") as? MyAdvertisementTableViewCell {
                    cell.viewModels = allMyAdvertisements
                    cell.headerLabel.text = "كل الإعلانات"
                    cell.delegate = self
                    cell.deletionDelegate = self
                    cell.soldDelegate = self
                    return cell
                }
            }
            else if selected == 1 {
                if let cell: MyAdvertisementTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyAdvertisementTableViewCell") as? MyAdvertisementTableViewCell {
                    cell.viewModels = allMyAdvertisements
                    cell.headerLabel.text = "كل الإعلانات"
                    cell.delegate = self
                    cell.deletionDelegate = self
                    cell.soldDelegate = self
                    cell.editDelegate = self
                    return cell
                }
            }
            else if selected == 2 {
                if let cell: SoldAdsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SoldAdsTableViewCell") as? SoldAdsTableViewCell {
                    cell.viewModels = self.soldAdvertisements
                    cell.headerLabel.text = "إعلاناتي المباعة"
                    cell.delegate = self
                    cell.deletionDelegate = self
                    return cell
                }
            }
            else if selected == 3 {
                if let cell: MyAdvertisementTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyAdvertisementTableViewCell") as? MyAdvertisementTableViewCell {
                    cell.viewModels = self.allMySpecialAdvertisements
                    cell.headerLabel.text = "الإعلانات المميزة"
                    cell.delegate = self
                    cell.deletionDelegate = self
                    cell.editDelegate = self
                    return cell
                }
            }
            else if selected == 4 {
                if let cell: OtherAdsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OtherAdsTableViewCell") as? OtherAdsTableViewCell {
                    cell.headerLabel.text = "إعلاناتي المفضلة"
                    cell.viewModels = self.savedAdvertisements
                    cell.delegate = self
                    cell.reloadDelegate = self
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                selected = 1
                tableView.reloadData()
            } else if indexPath.row == 1 {
                selected = 2
                tableView.reloadData()
            } else if indexPath.row == 2 {
                selected = 3
                tableView.reloadData()
            } else if indexPath.row == 3 {
                selected = 4
                tableView.reloadData()
            }
        }

        

    }
    
}

//MARK:- MyAdvertisementTableViewCellDelegate
extension AdvertisementsViewController: MyAdvertisementTableViewCellDelegate {
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
}

extension AdvertisementsViewController: OtherAdsTableViewCellDelegate {
    
    func didSelectOtherCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
}

extension AdvertisementsViewController: SoldAdsTableViewCellDelegate {
    
    func didSelectSoldAdCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
}

extension AdvertisementsViewController: ReloadSavedAdsTableViewCellDelegate {
    
    func didRemoveSavedAdvertisement() {
        getSavedAdvertisements()
    }
    
}

extension AdvertisementsViewController: ReloadAfterDeleteAdTableViewCellDelegate {
    
    func didRemoveAdvertisement() {
        getAllMyAdvertisements()
        getAllMySpecialAdvertisements()
    }
}

extension AdvertisementsViewController: ReloadAfterSellAdTableViewCellDelegate {
    
    func didSellAdvertisement() {
        getAllMyAdvertisements()
        getSoldAdvertisements()
    }
}

extension AdvertisementsViewController: ReloadAfterDeleteSoldAdTableViewCellDelegate {
    
    func didRemoveSoldAdvertisement() {
        getSoldAdvertisements()
    }
}

extension AdvertisementsViewController: GoToEditAdTableViewCellDelegate {
    
    func didEditAdvertisement(postId: Int) {
        self.editAdvertisementId = postId
        goToEditAddAdvertisementLevelOne()
    }
}
