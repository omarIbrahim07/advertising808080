//
//  EditProfileViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/15/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var newUserName: String?
    var newEmail: String?
    var newMobile: String?
    var newCountryCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "تعديل الملف الشخصي"
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TextEditTableViewCell", bundle: nil), forCellReuseIdentifier: "TextEditTableViewCell")
        tableView.register(UINib(nibName: "EditProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func editUserProfile() {
        
        guard let newUserName = self.newUserName, newUserName.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال إسم المستخدم الجديد"
            showError(error: apiError)
            return
        }
        
        guard let newEmail = self.newEmail, newEmail.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال البريد الإلكتروني الجديد"
            showError(error: apiError)
            return
        }
        
        guard let newMobile = self.newMobile, newMobile.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الهاتف الجديد"
            showError(error: apiError)
            return
        }
        
        let countryCode = "KW"
        
        print("name: \(newUserName)")
        print("email: \(newEmail)")
        print("phone: \(newMobile)")

        
        let parameters = [
            "name" : newUserName as AnyObject,
            "email" : newEmail as AnyObject,
            "phone" : newMobile as AnyObject,
            "country_code" : countryCode as AnyObject
            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AuthenticationAPIManager().changeUserProfile(basicDictionary: parameters, onSuccess: { (message) in
            print(message)
            
            weakSelf?.stopLoadingWithSuccess()
            
            UserDefaultManager.shared.currentUser!.name = self.newUserName
            UserDefaultManager.shared.currentUser!.email = self.newEmail
            UserDefaultManager.shared.currentUser!.phone = self.newMobile
            
            weakSelf?.presentHomeScreen()
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func presentEditPassword() {
        //        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        //        let viewController = storyboard.instantiateViewController(withIdentifier: "EditPasswordViewController")
        //        let naviagationController = UINavigationController(rootViewController: viewController)
        //        appDelegate.window!.rootViewController = naviagationController
        //        appDelegate.window!.makeKeyAndVisible()
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "EditPasswordViewController") as? EditPasswordViewController {
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
    @IBAction func editUserProfile(_ sender: Any) {
        editUserProfile()
    }
    

}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            if let cell: EditProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTableViewCell") as? EditProfileTableViewCell {
                
                cell.delegate = self
                
                if let userName = UserDefaultManager.shared.currentUser?.name {
                    cell.userNameLabel.text = userName
                }
                
                if let userImage = UserDefaultManager.shared.currentUser?.photo {
                    let imageURL = "http://new.808080group.com/storage/\(userImage)"
                    cell.userImage.loadImageFromUrl(imageUrl: imageURL)
                }
                
                if let userLastLogIn = UserDefaultManager.shared.currentUser?.lastLogInAt {
                    cell.hoursLabel.text = userLastLogIn
                }
                cell.editLabel.text = "تغيير كلمة السر"
                cell.option = false
                
                return cell
            }
        }
        
        else if indexPath.row == 1 {
            if let cell: TextEditTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextEditTableViewCell") as? TextEditTableViewCell {
                
                cell.headerLabel.text = "اسم المستخدم"
                cell.inputTextField.placeholder = "اسم المستخدم الجديد"
                cell.inputTextField.text = UserDefaultManager.shared.currentUser?.name
                cell.inputTextField.tag = 0
                cell.delegate = self
                
                return cell
            }
        }
        
        else if indexPath.row == 2 {
            if let cell: TextEditTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextEditTableViewCell") as? TextEditTableViewCell {
                
                cell.headerLabel.text = "البريد الإلكتروني"
                cell.inputTextField.placeholder = "البريد الإلكتروني الجديد"
                cell.inputTextField.text = UserDefaultManager.shared.currentUser?.email
                cell.inputTextField.tag = 1
                cell.delegate = self
                
                return cell
            }
        }
        
        else if indexPath.row == 3 {
            if let cell: TextEditTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextEditTableViewCell") as? TextEditTableViewCell {
                
                cell.headerLabel.text = "رقم الهاتف"
                cell.inputTextField.placeholder = "رقم الهاتف الجديد"
                cell.inputTextField.text = UserDefaultManager.shared.currentUser?.phone
                cell.inputTextField.tag = 2
                cell.delegate = self
                
                return cell
            }
        }
        
//        else if indexPath.row == 3 {
//            if let cell: TextEditTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextEditTableViewCell") as? TextEditTableViewCell {
//
//                cell.headerLabel.text = "البلد"
//                cell.inputTextField.placeholder = "البلد الجديد"
//                cell.inputTextField.tag = 3
//                cell.delegate = self
//
//                return cell
//            }
//        }
        
        return UITableViewCell()
    }
    
    
    
    
}

extension EditProfileViewController: EditProfileTableViewCellDelegate {
    func didProfileTextFieldEditted(tag: Int, text: String) {
        if tag == 0 {
            self.newUserName = text
            print(self.newUserName as Any)
        } else if tag == 1 {
            self.newEmail = text
            print(self.newEmail as Any)
        } else if tag == 2 {
            self.newMobile = text
            print(self.newMobile as Any)
        }
    }
    
    
}

extension EditProfileViewController: EditProfileButtonTableViewCellDelegate {
    
    func didEditProfilePressedButton(choosed: String) {
        if choosed == "EditPassword" {
            self.presentEditPassword()
        }
    }
}
