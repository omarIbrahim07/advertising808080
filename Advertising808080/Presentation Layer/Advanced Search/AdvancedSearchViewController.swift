//
//  AdvancedSearchViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/18/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class AdvancedSearchViewController: BaseViewController {
    
    var mainCategories: [MainCategories] = []
    
    var advertisementTitle: String?
    var advertisementLeastPrice: String?
    var advertisementMaxPrice: String?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureTableView()
        getMainCategories()
        // Do any additional setup after loading the view.
    }
    
    func configureUI() {
        searchButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        searchButton.addCornerRadius(raduis: searchButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "AdvancedPickerViewTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvancedPickerViewTableViewCell")
        tableView.register(UINib(nibName: "AdvancedTextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvancedTextFieldTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getMainCategories() {
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        CategoriesAPIManager().getMainCategories(basicDictionary: [:], onSuccess: { (Categories) in
            self.mainCategories = Categories
            
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    @IBAction func searchButtonIsPressed(_ sender: Any) {
        print("Pressed")
    }
    
    

}


extension AdvancedSearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: AdvancedTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedTextFieldTableViewCell") as? AdvancedTextFieldTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "عنوان الإعلان"
                cell.inputTextField.placeholder = "عنوان الإعلان"
                cell.inputTextField.tag = 0
                
                return cell
            }
        }
            
        else if indexPath.row == 1 {
            if let cell: AdvancedTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedTextFieldTableViewCell") as? AdvancedTextFieldTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "أقل سعر"
                cell.inputTextField.placeholder = "أقل سعر"
                cell.inputTextField.tag = 1
                                
                return cell
            }
        }
            
            
        else if indexPath.row == 2 {
            if let cell: AdvancedTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedTextFieldTableViewCell") as? AdvancedTextFieldTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "أعلى سعر"
                cell.inputTextField.placeholder = "أعلى سعر"
                cell.inputTextField.tag = 2
                
                return cell
            }
        }
        
        else if indexPath.row == 3 {
            if let cell: AdvancedPickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedPickerViewTableViewCell") as? AdvancedPickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "نوع الإعلان"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 3
                
                return cell
            }
        }
            
        else if indexPath.row == 4 {
            if let cell: AdvancedPickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedPickerViewTableViewCell") as? AdvancedPickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "التصنيفات"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 4
                cell.viewModelsMainCategories = self.mainCategories
                
                return cell
            }
        }
        
        else if indexPath.row == 5 {
            if let cell: AdvancedPickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedPickerViewTableViewCell") as? AdvancedPickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "الحالة"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 5
                
                return cell
            }
        }
        
        else if indexPath.row == 6 {
            if let cell: AdvancedPickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedPickerViewTableViewCell") as? AdvancedPickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "الضمان"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 6
                
                return cell
            }
        }
        
        else if indexPath.row == 7 {
            if let cell: AdvancedPickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvancedPickerViewTableViewCell") as? AdvancedPickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "العملة"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 7
                
                return cell
            }
        }
        
        
        
        return UITableViewCell()
    }
    
    
    
}

extension AdvancedSearchViewController: AdvancedSearchTextFieldTableViewCellDelegate {
    
    func didAdvancedSearchTextFieldEditted(tag: Int, text: String) {
        if tag == 0 {
            self.advertisementTitle = text
            print(self.advertisementTitle as Any)
        } else if tag == 1 {
            self.advertisementLeastPrice = text
            print(self.advertisementLeastPrice as Any)
        } else if tag == 2 {
            self.advertisementMaxPrice = text
            print(self.advertisementMaxPrice as Any)
        }
    }
    
    
}

extension AdvancedSearchViewController: AdvancedPickerViewTableViewCellDelegate {
    
    
    func didSelectAdvancedPickerView(tag: Int, id: Int) {
        if tag == 3 {
            print("7obby1")
//            self.categoryID = id
//            getSubCategories()
//            print(self.categoryID as Any)
        } else if tag == 4 {
            print("7obby2")
//            self.subCategoryId = id
//            tableView.reloadData()
//            print(self.subCategoryId as Any)
        } else if tag == 5 {
            print("7obby3")
            //            self.subCategoryId = id
            //            tableView.reloadData()
            //            print(self.subCategoryId as Any)
        } else if tag == 6 {
            print("7obby4")
            //            self.subCategoryId = id
            //            tableView.reloadData()
            //            print(self.subCategoryId as Any)
        } else if tag == 7 {
            print("7obby5")
            //            self.subCategoryId = id
            //            tableView.reloadData()
            //            print(self.subCategoryId as Any)
        }
    }
    
    
}
