//
//  AdvancedTextFieldTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/19/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol AdvancedSearchTextFieldTableViewCellDelegate {
    func didAdvancedSearchTextFieldEditted(tag: Int,text: String)
}

class AdvancedTextFieldTableViewCell: UITableViewCell {
    
    var delegate: AdvancedSearchTextFieldTableViewCellDelegate?
    
    //MARK:- Outlets
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        inputTextField.delegate = self
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didAdvancedSearchTextFieldEditted(tag: Int,text: String) {
        if let delegateValue = delegate {
            delegateValue.didAdvancedSearchTextFieldEditted(tag: tag,text: text)
        }
    }
    
}

extension AdvancedTextFieldTableViewCell: UITextFieldDelegate {
    
        func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
            if inputTextField.tag == 0 {
                didAdvancedSearchTextFieldEditted(tag: 0, text: inputTextField.text!)
            } else if inputTextField.tag == 1 {
                didAdvancedSearchTextFieldEditted(tag: 1, text: inputTextField.text!)
            } else if inputTextField.tag == 2 {
                didAdvancedSearchTextFieldEditted(tag: 2, text: inputTextField.text!)
            }
        }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if inputTextField.tag == 0 {
//            didAdvancedSearchTextFieldEditted(tag: 0, text: inputTextField.text!)
//        } else if inputTextField.tag == 1 {
//            didAdvancedSearchTextFieldEditted(tag: 1, text: inputTextField.text!)
//        } else if inputTextField.tag == 2 {
//            didAdvancedSearchTextFieldEditted(tag: 2, text: inputTextField.text!)
//        }
//        return true
//    }
    
}
