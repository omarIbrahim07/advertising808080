//
//  AdvancedPickerViewTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/19/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol AdvancedPickerViewTableViewCellDelegate {
    func didSelectAdvancedPickerView(tag: Int,id: Int)
}

class AdvancedPickerViewTableViewCell: UITableViewCell {
    
    var delegate: AdvancedPickerViewTableViewCellDelegate?
    
    var Selected: String?
    
    var mainCategoriesTitles: [String] = []
    var mainCategoriesIndexs : [Int] = []
    
    var viewModelsMainCategories : [MainCategories] = []{
        didSet {
            getMainCategories()
        }
    }
    
    func getMainCategories() {
        
        for category in self.viewModelsMainCategories {
            self.mainCategoriesTitles.append(category.name)
            self.mainCategoriesIndexs.append(category.id)
        }
        
    }
    
    let costType = ["ثابت",
                    "قابل للتفاوض",
                    "الثمن عند الاتصال",
                    "مزاد علني",
                    "مجاني",
                    "ثمن غير محدد"]
    
    let currency = [
        "$",
        "§",
        ]
    
    let state = [
        "جديد",
        "مستعمل"
    ]
    
    let insurance = [
        "يوجد",
        "لا يوجد"
    ]
    
    let advertisementType = [
        "مميز",
        "بسيط"
    ]
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        configureTextFields()
        createDayPicker()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
    
    func configureTextFields() {
        configureTextField(textField: textField, imageName: "icons8-new-post-50")
    }
    
    func configureTextField(textField: UITextField, imageName: String) {
        textField.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 189, y: 0, width: 20, height: 20))
        let textFieldImage = UIImage(named: imageName)
        imageView.image = textFieldImage
        textField.rightView = imageView
    }
    
    func createDayPicker() {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        textField.inputView = Picker
    }

    //MARK:- Delegate Helpers
    func didSelectAdvancedPickerView(tag: Int,id: Int) {
        if let delegateValue = delegate {
            delegateValue.didSelectAdvancedPickerView(tag: tag,id: id)
        }
    }
    
}


extension AdvancedPickerViewTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if textField.tag == 3 {
            return advertisementType.count
        } else if textField.tag == 4 {
            return mainCategoriesTitles.count
        } else if textField.tag == 5 {
            return state.count
        } else if textField.tag == 6 {
            return insurance.count
        } else if textField.tag == 7 {
            return currency.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if textField.tag == 3 {
            return advertisementType[row]
        } else if textField.tag == 4 {
            return mainCategoriesTitles[row]
        } else if textField.tag == 5 {
            return state[row]
        } else if textField.tag == 6 {
            return insurance[row]
        } else if textField.tag == 7 {
            return currency[row]
        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
        if textField.tag == 3 {
            
            Selected = advertisementType[row]
            textField.text = Selected
            didSelectAdvancedPickerView(tag: 3,id: 0)
            self.view.endEditing(true)
        } else if textField.tag == 4 {
            
            Selected = mainCategoriesTitles[row]
            textField.text = Selected
            didSelectAdvancedPickerView(tag: 4,id: mainCategoriesIndexs[row])
            self.view.endEditing(true)
        } else if textField.tag == 5 {
            
            Selected = state[row]
            textField.text = Selected
            didSelectAdvancedPickerView(tag: 5,id: 0)
            self.view.endEditing(true)
        } else if textField.tag == 6 {
            
            Selected = insurance[row]
            textField.text = Selected
            didSelectAdvancedPickerView(tag: 6,id: 0)
            self.view.endEditing(true)
        } else if textField.tag == 7 {
            
            Selected = currency[row]
            textField.text = Selected
            didSelectAdvancedPickerView(tag: 7,id: 0)
            self.view.endEditing(true)
        }
    }
    
    
    
    
}
