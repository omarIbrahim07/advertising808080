//
//  AdvertisementDetailsViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/13/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import MessageUI

class AdvertisementDetailsViewController: BaseViewController, MFMessageComposeViewControllerDelegate {
    
    //MARK:- Variables
    var advertisementDetails: AdvertisementDetailsModel?
    
    var secondaryAdvertisementDetails: SecondaryCategories?
    
    var similarAdvertisements: [AdvertisementDetailsModel] = []
    
    var advertisementState: String?
    
    var advertisementID: String?
    var isSaved: Int?
    
    var imageNames : [Picture] = []
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "تفاصيل الإعلان"
        configureTableView()
        getAdvertisementDetails()
        getSimilarAdvertisements()
        checkSavedPost()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Configuration UI
    
    func configureTableView() {
        tableView.register(UINib(nibName: "AdvertisementDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementDetailsTableViewCell")
        tableView.register(UINib(nibName: "AdvertisementDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementDetailsTableViewCell")
        tableView.register(UINib(nibName: "SimilarAdvertisingTableViewCell", bundle: nil), forCellReuseIdentifier: "SimilarAdvertisingTableViewCell")
        tableView.register(UINib(nibName: "ThreeButtonsDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ThreeButtonsDetailsTableViewCell")
        tableView.register(UINib(nibName: "AdvertisementRollerImagesTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementRollerImagesTableViewCell")
        tableView.register(UINib(nibName: "AdvertisementDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementDescriptionTableViewCell")
        tableView.register(UINib(nibName: "AdvertisementOwnerDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementOwnerDetailsTableViewCell")
        tableView.register(UINib(nibName: "SpaceTableViewCell", bundle: nil), forCellReuseIdentifier: "SpaceTableViewCell")
        tableView.register(UINib(nibName: "ConnectUserTableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectUserTableViewCell")
        tableView.register(UINib(nibName: "BiddingTableViewCell", bundle: nil), forCellReuseIdentifier: "BiddingTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getAdvertisementDetails() {
        
        self.startLoading()
        
        CategoriesAPIManager().getAdvertisementDetails(Parent: advertisementID!, basicDictionary: [:], onSuccess: { (advertisementDetails) in
            
            self.advertisementDetails = advertisementDetails
            
            if let advertisementImages = advertisementDetails.advertisementImages {
                self.imageNames = advertisementImages
            }
            
            if let BannerImage = self.advertisementDetails?.advertisementBanner {
                self.bannerImage.loadImageFromUrl(imageUrl: BannerImage)
            }
            
            guard let postID = self.advertisementDetails?.id, postID > 0 else {
                let apiError = APIError()
                apiError.message = "غير قادر"
                self.showError(error: apiError)
                return
            }
            
            print("postID: \(postID)")
            
            
            let parameters = [
                "postId" : postID as AnyObject,
                ]
            
            weak var weakSelf = self
            
            AdvertisementsAPIManager().checkSaveAdvertisement(basicDictionary: parameters, onSuccess: { (message) in
                print(message)
                
                if message == 1 {
                    self.isSaved = message
                    print("تم الحفظ")
                } else if message == 0 {
                    self.isSaved = message
                    print("تم إلغاء الحفظ")
                }
                
                self.tableView.reloadData()
                
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
            
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    
    
    func getSimilarAdvertisements() {
        
        CategoriesAPIManager().getSimilarAdvertisements(postID: advertisementID!, basicDictionary: [:], onSuccess: { (similarAdvertisements) in
            
            self.similarAdvertisements = similarAdvertisements
            
            self.tableView.reloadData()
                        
        }) { (error) in
            print("error")
        }
        
    }
    
    func savePost() {
        
        guard let postID = self.advertisementDetails?.id, postID > 0 else {
            let apiError = APIError()
            apiError.message = "غير قادر"
            showError(error: apiError)
            return
        }
        
        print("postID: \(postID)")
        
        
        let parameters = [
            "postId" : postID as AnyObject,
            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AdvertisementsAPIManager().saveAdvertisement(basicDictionary: parameters, onSuccess: { (message) in
            print(message)
            
            weakSelf?.stopLoadingWithSuccess()
            
            if message == 1 {
                print("تم الحفظ")
                self.isSaved = message
            } else if message == 0 {
                print("تم إلغاء الحفظ")
                self.isSaved = message
            }
            
            self.tableView.reloadData()
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func checkSavedPost() {
        
//        guard let postID = self.advertisementDetails?.id, postID > 0 else {
//            let apiError = APIError()
//            apiError.message = "غير قادر"
//            showError(error: apiError)
//            return
//        }
//
//        print("postID: \(postID)")
//
//
//        let parameters = [
//            "postId" : postID as AnyObject,
//            ]
//
//        weak var weakSelf = self
//        weakSelf?.startLoading()
//        AdvertisementsAPIManager().checkSaveAdvertisement(basicDictionary: parameters, onSuccess: { (message) in
//            print(message)
//
//            weakSelf?.stopLoadingWithSuccess()
//
//            if message == 1 {
//                self.isSaved = message
//                print("تم الحفظ")
//            } else if message == 0 {
//                self.isSaved = message
//                print("تم إلغاء الحفظ")
//            }
//
//            self.tableView.reloadData()
//
//
//        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
//        }
        
    }
    
    
    func showCallAlert() {
        
        guard let userPhone = UserDefaultManager.shared.currentUser?.phone, let url = URL(string: "telprompt://\(userPhone)") else {
            return
        }
        
        // create the alert
        let alert = UIAlertController(title: "هل تريد الإتصال ؟", message: userPhone, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "إتصال", style: UIAlertAction.Style.destructive, handler: { action in
            UIApplication.shared.canOpenURL(url)
        }))
        alert.addAction(UIAlertAction(title: "إلغاء", style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }

    func showMessageMessageAlert() {
        
        guard let userPhone = UserDefaultManager.shared.currentUser?.phone, let _ = URL(string: "telprompt://\(userPhone)") else {
            return
        }
        
        // create the alert
        let alert = UIAlertController(title: "هل تريد إرسال رسالة ؟", message: userPhone, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "نعم", style: UIAlertAction.Style.destructive, handler: { action in
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = "Message Body"
                controller.recipients = [userPhone]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "إلغاء", style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK:- TableView
extension AdvertisementDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if indexPath.row == 0 {
            if let cell: AdvertisementRollerImagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementRollerImagesTableViewCell") as? AdvertisementRollerImagesTableViewCell {
                cell.viewModels = self.imageNames
                return cell
            }
        }
        
        else if indexPath.row == 1 {
            if let cell: SpaceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SpaceTableViewCell") as? SpaceTableViewCell {
                return cell
            }
        }
            
        else if indexPath.row == 2 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "عنوان الإعلان"
                cell.subLabel.text = "لا يوجد"
                if let title = advertisementDetails?.title {
                    cell.subLabel.text = title
                }
                return cell
            }
        }
            
        else if indexPath.row == 3 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "السعر"
                cell.subLabel.text = "لا يوجد"
                cell.subLabel.textColor = #colorLiteral(red: 0.262745098, green: 0.4039215686, blue: 0.6980392157, alpha: 1)
                if let price = advertisementDetails?.price {
                    cell.subLabel.text = price
                }
                return cell
            }
        }
            
        else if indexPath.row == 4 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "الموقع"
                cell.subLabel.text = "لا يوجد"
                if let address = advertisementDetails?.city {
                    cell.subLabel.text = address
                }
                return cell
            }
        }
            
        else if indexPath.row == 5 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "الضمان"
                cell.subLabel.text = "لا يوجد"
                return cell
            }
        }
            
        else if indexPath.row == 6 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "التاريخ"
                cell.subLabel.text = "لا يوجد"
                if let date = advertisementDetails?.advertisementTime {
                    cell.subLabel.text = date
                }
                return cell
            }
        }
            
        else if indexPath.row == 7 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "عدد المشاهدات"
                cell.subLabel.text = "لا يوجد"
                if let visitNumber = advertisementDetails?.visits {
                    cell.subLabel.text = visitNumber
                }
                return cell
            }
        }
            
        else if indexPath.row == 8 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "الحالة"
                cell.subLabel.text = "لا يوجد"
                return cell
            }
        }
            
        else if indexPath.row == 9 {
            if let cell: AdvertisementDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDetailsTableViewCell") as? AdvertisementDetailsTableViewCell {
                cell.mainLabel.text = "رابط اليوتيوب"
                cell.subLabel.text = "لا يوجد"
                cell.subLabel.textColor = #colorLiteral(red: 0.262745098, green: 0.4039215686, blue: 0.6980392157, alpha: 1)
                return cell
            }
        }
            
        else if indexPath.row == 10 {
            if let cell: SpaceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SpaceTableViewCell") as? SpaceTableViewCell {
                return cell
            }
        }
            
        else if indexPath.row == 11 {
            if let cell: ThreeButtonsDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ThreeButtonsDetailsTableViewCell") as? ThreeButtonsDetailsTableViewCell {
                cell.delegate = self
                if isSaved == 1 {
                    print("Liked")
                    cell.likeImage.image = UIImage(named: "filled")
                } else if isSaved == 0 {
                    print("UnLiked")
                    cell.likeImage.image = UIImage(named: "favorite")
                }
                return cell
            }
        }
        
        else if indexPath.row == 12 {
            if let cell: AdvertisementDescriptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementDescriptionTableViewCell") as? AdvertisementDescriptionTableViewCell {
                cell.descriptionLabel.text = "لا يوجد"
                if let description = advertisementDetails?.description {
                    if description.first == "<" {
                        cell.descriptionLabel.attributedText = description.html2Attributed
//                        cell.descriptionLabel.font = UIFont(name:"Noto Kufi Arabic-Bold",size:15)
                        cell.descriptionLabel.font = UIFont.boldSystemFont(ofSize: 15)
                        cell.descriptionLabel.numberOfLines = 0
                        cell.descriptionLabel.textAlignment = .right
                    } else {
                        cell.descriptionLabel.text = description
                    }
                }
                return cell
            }
        }
        
        else if indexPath.row == 13 {
            if let cell: AdvertisementOwnerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementOwnerDetailsTableViewCell") as? AdvertisementOwnerDetailsTableViewCell {
                cell.userId = advertisementDetails?.userId
                cell.postId = advertisementDetails?.id
                cell.delegate = self
                cell.chatDelegate = self
                
                cell.userNameLabel.text = "لا يوجد"
                if let userName = advertisementDetails?.contactName {
                    cell.userNameLabel.text = userName
                }
                
                cell.hoursLabel.text = "لا يوجد آخر تسجيل دخول"
//                if let createdAt = advertisementDetails?.createdAt {
//                    cell.hoursLabel.text = createdAt
//                }
                
//                if let userImage = advertisementDetails.userImage {
//                    cell.userImage.loadImageFromUrl(imageUrl: "http://new.808080group.com/storage/\(userImage)")
//                }
                
                return cell
            }
        }
            
        else if indexPath.row == 14 {
            if let cell: SimilarAdvertisingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SimilarAdvertisingTableViewCell") as? SimilarAdvertisingTableViewCell {
                cell.viewModels = self.similarAdvertisements
                cell.delegate = self
                return cell
            }
        }
        
        else if indexPath.row == 15 {
            if let cell: ConnectUserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ConnectUserTableViewCell") as? ConnectUserTableViewCell {
                cell.delegate = self
                return cell
            }
        }


        return UITableViewCell()
    }
    
    
}

//MARK:- MyAdvertisementTableViewCellDelegate
extension AdvertisementDetailsViewController: SimilarAdvertisingTableViewCellDelegate {
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
}

extension AdvertisementDetailsViewController: AdvertisementOwnerDetailsTableViewCellDelegate {
    
    
//    func didSelectAdvertisementOwnerTableViewCell(Id: String) {
//        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ViewAdvertisementUserViewController") as? ViewAdvertisementUserViewController {
//            if "\(UserDefaultManager.shared.currentUser?.userID ?? "0")" != Id {
//                viewController.userId = Id
//                print(Id)
//                self.navigationController?.show(viewController, sender: self)
//            }
//        }
//    }
    
        func didSelectAdvertisementOwnerTableViewCell(Id: String) {
            if let viewController = storyboard?.instantiateViewController(withIdentifier: "ViewAdvertisementUserViewController") as? ViewAdvertisementUserViewController {
                    viewController.userId = Id
                    print(Id)
                    self.navigationController?.show(viewController, sender: self)
            }
        }

}

extension AdvertisementDetailsViewController: chatWithAdvertisementOwnerTableViewCellDelegate {
    func didPressChatButtonTableViewCell(userId: String, postId: Int) {
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ChatRoomViewController") as? ChatRoomViewController {
            viewController.advertisementId = String(postId)
            viewController.toUserId = userId
            print(postId)
            print(userId)
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
    
    
    //    func didSelectAdvertisementOwnerTableViewCell(Id: String) {
    //        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ViewAdvertisementUserViewController") as? ViewAdvertisementUserViewController {
    //            if "\(UserDefaultManager.shared.currentUser?.userID ?? "0")" != Id {
    //                viewController.userId = Id
    //                print(Id)
    //                self.navigationController?.show(viewController, sender: self)
    //            }
    //        }
    //    }
    
//    func didSelectAdvertisementOwnerTableViewCell(Id: String) {
//        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ViewAdvertisementUserViewController") as? ViewAdvertisementUserViewController {
//            viewController.userId = Id
//            print(Id)
//            self.navigationController?.show(viewController, sender: self)
//        }
//    }
    
}

extension AdvertisementDetailsViewController: ConnectUserTableViewCellDelegate {
    
    func didSelectButton(pressed: String) {
        if pressed == "Call" {
            showCallAlert()
        } else if pressed == "Message" {
            showMessageMessageAlert()
            print("حبي")
        }
    }
}

extension AdvertisementDetailsViewController: ThreeButtonsDetailsTableViewCellDelegate {
    
    func didPressedButton(pressed: String) {
        if pressed == "Saved" {
            self.savePost()
        } else if pressed == "Report" {
            
        } else if pressed == "Share" {
            let firstActivityItem = "Text you want"
            
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [firstActivityItem], applicationActivities: nil)
            
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    
}

extension String {
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }


}


