//
//  AdvertisementOwnerDetailsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/14/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import Cosmos


protocol AdvertisementOwnerDetailsTableViewCellDelegate {
    func didSelectAdvertisementOwnerTableViewCell(Id: String)
}

protocol chatWithAdvertisementOwnerTableViewCellDelegate {
    func didPressChatButtonTableViewCell(userId: String, postId: Int)
}

class AdvertisementOwnerDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var cellChoosedButton: UIButton!
    
    var delegate: AdvertisementOwnerDetailsTableViewCellDelegate?
    var chatDelegate: chatWithAdvertisementOwnerTableViewCellDelegate?
    
    var userId: String?
    var postId: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        selectionStyle = .none
        // Initialization code
    }

    func configureUI() {
        userImage.addCornerRadius(raduis: userImage.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didSelectAdvertisementOwnerTableViewCell(Id: String) {
        if let delegateValue = delegate {
            delegateValue.didSelectAdvertisementOwnerTableViewCell(Id: Id)
        }
    }
    
    func didPressChatButtonTableViewCell(userId: String, postId: Int) {
        if let delegateValue = chatDelegate {
            delegateValue.didPressChatButtonTableViewCell(userId: userId, postId: postId)
        }
    }
    
    @IBAction func visitProfileButtonPressed(_ sender: Any) {
        didSelectAdvertisementOwnerTableViewCell(Id: userId!)
        print("Visit pressed")
    }
    
    @IBAction func chatButtonIsPressed(_ sender: Any) {
        didPressChatButtonTableViewCell(userId: self.userId!, postId: self.postId!)
    }
    
    
    
}
