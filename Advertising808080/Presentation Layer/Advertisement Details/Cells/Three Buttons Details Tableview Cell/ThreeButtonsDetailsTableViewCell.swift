//
//  ThreeButtonsDetailsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/17/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol ThreeButtonsDetailsTableViewCellDelegate {
    func didPressedButton(pressed: String)
}

class ThreeButtonsDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var reportView: UIView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var shareView: UIView!
    
    var delegate: ThreeButtonsDetailsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUI()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureUI() {
        reportView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8472429514, green: 0.8472628593, blue: 0.8472521305, alpha: 1), borderWidth: 2)
        likeView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8472429514, green: 0.8472628593, blue: 0.8472521305, alpha: 1), borderWidth: 2)
        shareView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8472429514, green: 0.8472628593, blue: 0.8472521305, alpha: 1), borderWidth: 2)
    }
    
    //MARK:- Delegate Helpers
    func didPressedButton(pressed: String) {
        if let delegateValue = delegate {
            delegateValue.didPressedButton(pressed: pressed)
        }
    }
    
    @IBAction func reportButtonIsPressed(_ sender: Any) {
        print("Report")
        didPressedButton(pressed: "Report")
    }
    
    @IBAction func saveButtonIsPressed(_ sender: Any) {
        print("Saved")
        didPressedButton(pressed: "Saved")
    }
    
    
    @IBAction func shareButtonIsPressed(_ sender: Any) {
        print("Share")
        didPressedButton(pressed: "Share")
    }
}
