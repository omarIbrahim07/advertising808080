//
//  AdvertisementRollerImagesTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/14/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import FSPagerView

//protocol AdvertisementRollerImagesTableViewCellDelegate {
//    func didSetArrayOfImages(viewModel: [String])
//}

class AdvertisementRollerImagesTableViewCell: UITableViewCell {
    
//    fileprivate let imageNames = ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg"]
    var imageNames : [String] = []
    var numberOfItems : Int?

    var viewModels : [Picture] = []{
        didSet {
            getPictrues()
        }
    }
    
    func getPictrues() {
        
        imageNames = []
        
        for image in self.viewModels {
            let newImage = "http://new.808080group.com/storage/"+String(image.image)
            self.imageNames.append(newImage)
            print(newImage)
        }
        numberOfItems = imageNames.count
        pagerView.reloadData()
//        pageControl.reloadInputViews()
    }
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: "cell", bundle: nil), forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = FSPagerView.automaticSize
        }
    }
    
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = self.imageNames.count
//                        self.pageControl.numberOfPages = 1
            self.pageControl.contentHorizontalAlignment = .right
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureFSPageControl()
    }
    
    func configureFSPageControl() {
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.reloadData()
    }
    
}


extension AdvertisementRollerImagesTableViewCell: FSPagerViewDataSource,FSPagerViewDelegate {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        if let numberOfItems = self.numberOfItems {
            return numberOfItems
        }
        return 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! cell
        cell.viewModels = imageNames[index]
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        switch sender.tag {
        case 1:
            let newScale = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
        case 2:
            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
        case 3:
            self.numberOfItems = Int(roundf(sender.value*7.0))
            if let numberOfPages = self.numberOfItems {
                self.pageControl.numberOfPages = numberOfPages
            }
            self.pagerView.reloadData()
        default:
            break
        }
}

}
