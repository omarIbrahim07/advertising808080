//
//  SimilarAdvertisingTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/17/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol SimilarAdvertisingTableViewCellDelegate {
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

class SimilarAdvertisingTableViewCell: UITableViewCell {
    
    var delegate: SimilarAdvertisingTableViewCellDelegate?

    @IBOutlet weak var similarAdvertisementsCollectionView: UICollectionView!
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.similarAdvertisementsCollectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCollectionView() {
        similarAdvertisementsCollectionView.register(UINib(nibName: "SpecialAdvertisementCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SpecialAdvertisementCollectionViewCell")
        similarAdvertisementsCollectionView.delegate = self
        similarAdvertisementsCollectionView.dataSource = self
        similarAdvertisementsCollectionView.reloadData()
    }
    
    //MARK:- Delegate Helpers
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectCollectionViewCell(viewModel: viewModel)
        }
    }
    
}


extension SimilarAdvertisingTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: SpecialAdvertisementCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialAdvertisementCollectionViewCell", for: indexPath) as? SpecialAdvertisementCollectionViewCell {
            
            cell.similarAdvertisementViewModel = viewModels[indexPath.row]

            cell.specialAdvertisementLabel.isHidden = true
            cell.view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), borderWidth: 0)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
    
}
