//
//  ConnectUserTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//



import UIKit

protocol ConnectUserTableViewCellDelegate {
    func didSelectButton(pressed: String)
}

class ConnectUserTableViewCell: UITableViewCell {

    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var messageView: UIView!
    
    var delegate: ConnectUserTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        callView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        messageView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    //MARK:- Delegate Helpers
    func didSelectButton(pressed: String) {
        if let delegateValue = delegate {
            delegateValue.didSelectButton(pressed: pressed)
        }
    }
    
    @IBAction func callButtonIsPressed(_ sender: Any) {
        print("Call is pressed")
        didSelectButton(pressed: "Call")
    }
    
    @IBAction func messageButtonIsPressed(_ sender: Any) {
        print("Message is pressed")
        didSelectButton(pressed: "Message")
    }
    
    
}
