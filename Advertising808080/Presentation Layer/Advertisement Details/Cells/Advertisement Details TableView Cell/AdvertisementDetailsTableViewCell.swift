//
//  AdvertisementDetailsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/14/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class AdvertisementDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
