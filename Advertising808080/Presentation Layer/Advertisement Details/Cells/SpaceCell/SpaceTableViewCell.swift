//
//  SpaceTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/18/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SpaceTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
