//
//  cell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/14/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import FSPagerView

class cell: FSPagerViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var viewModels : String? {
        didSet {
            bindData()
        }
    }
    
    func bindData() {
        
        if let image = viewModels {
            self.image.loadImageFromUrl(imageUrl: image)
        }
        
    }
  
    
}
