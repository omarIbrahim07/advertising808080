//
//  AdvertisementDescriptionTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/17/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class AdvertisementDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
        selectionStyle = .none
    }

    func configureUI() {
        view.addCornerRadius(raduis: 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
}
