//
//  SearchingViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/24/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SearchingViewController: BaseViewController {

    var searchedAdvertisements: [AdvertisementDetailsModel] = []
    
    var advertisementID: String?
    var searchedString: String?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getSearchedAdvertisements()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "AdvertisementShowTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementShowTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getSearchedAdvertisements() {
        
        guard let searchedString = self.searchedString else{
            return
        }
        
        let parameters: [String:String] = ["keyword" : searchedString]
        
        self.startLoading()
        
        CategoriesAPIManager().getSearchedAdvertisements(basicDictionary: parameters as [String : AnyObject], onSuccess: { (searchedAdvertisements) in
            self.searchedAdvertisements = searchedAdvertisements
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    //MARK:- Segue to Advertisement Details
    func goToAdvertisementDetails(parent: String) {
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementID = parent
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
}

extension SearchingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchedAdvertisements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: AdvertisementShowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementShowTableViewCell") as? AdvertisementShowTableViewCell {
            
            if let advertisementImage = self.searchedAdvertisements[indexPath.row].advertisementImage {
                let adImage = "http://new.808080group.com/storage/"+String(advertisementImage)
                print(adImage)
                cell.advertisementImage.loadImageFromUrl(imageUrl: adImage)
            }
            
            cell.advertisementName.text = "لا يوجد عنوان للإعلان"
            if let advertiseName = self.searchedAdvertisements[indexPath.row].title {
                cell.advertisementName.text = advertiseName
            }
            
            cell.advertisementPrice.text = "لا يوجد سعر للإعلان"
            if let advertisePrice = self.searchedAdvertisements[indexPath.row].price {
                cell.advertisementPrice.text = advertisePrice
            }
            
            cell.advertisementTime.text = "لا يوجد"
            if let advertiseTime = self.searchedAdvertisements[indexPath.row].createdAt {
                cell.advertisementTime.text = advertiseTime
            }
            
            cell.advertisementPlace.text = "لا يوجد مكان للإعلان"
            if let advertisePlace = self.searchedAdvertisements[indexPath.row].city {
                cell.advertisementPlace.text = advertisePlace
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        advertisementID = String(searchedAdvertisements[indexPath.row].id)
        goToAdvertisementDetails(parent: advertisementID!)
    }
    
    
}


