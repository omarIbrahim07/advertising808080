//
//  SideMenuBarTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SideMenuBarTableViewCell: UITableViewCell {

    @IBOutlet weak var menuIconImage: UIImageView!
    @IBOutlet weak var menuIconLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
