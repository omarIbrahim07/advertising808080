//
//  SideMenuBarViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SideMenuBarViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "SideMenuBarTableViewCell", bundle: nil), forCellReuseIdentifier: "SideMenuBarTableViewCell")
        tableView.register(UINib(nibName: "ImageSideMenuBarTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageSideMenuBarTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
}

extension SideMenuBarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: ImageSideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImageSideMenuBarTableViewCell") as? ImageSideMenuBarTableViewCell {
                cell.menuBannerImage.image = UIImage(named: "front_app")
                return cell
            }
        }
        
        if indexPath.row == 1 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "الرئيسية"
                cell.menuIconImage.image = UIImage(named: "main")
                return cell
            }
        }
        
        else if indexPath.row == 2 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "الملف الشخصي"
                cell.menuIconImage.image = UIImage(named: "profile")
                return cell
            }
        }
        
        else if indexPath.row == 3 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "بحث متقدم"
                cell.menuIconImage.image = UIImage(named: "advanced")
                return cell
            }
        }
        
        else if indexPath.row == 4 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "الرسائل"
                cell.menuIconImage.image = UIImage(named: "messages")
                return cell
            }
        }
        
        else if indexPath.row == 5 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "إعلاناتي"
                cell.menuIconImage.image = UIImage(named: "my_ads")
                return cell
            }
        }
        
        else if indexPath.row == 6 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "إعلاناتي المميزة"
                cell.menuIconImage.image = UIImage(named: "stared")
                return cell
            }
        }
        
        else if indexPath.row == 7 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "إعلاناتي المفضلة"
                cell.menuIconImage.image = UIImage(named: "favorite")
                return cell
            }
        }
        
        else if indexPath.row == 8 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "كل الإعلانات"
                cell.menuIconImage.image = UIImage(named: "select-all")
                return cell
            }
        }
        
        else if indexPath.row == 9 {
            if let cell: SideMenuBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuBarTableViewCell") as? SideMenuBarTableViewCell {
                cell.menuIconLabel.text = "تسجيل الخروج"
                cell.menuIconImage.image = UIImage(named: "logout")
                return cell
            }
        }
        
        
        

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            if let homeNavigationController = storyboard?.instantiateViewController(withIdentifier: "HomeViewControllerNavigationController") as? UINavigationController, let rootViewContoller = homeNavigationController.viewControllers[0] as? HomeViewController {
//                rootViewContoller.test = "test String"
                self.present(homeNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 2 {
            if let profileNavigationController = storyboard?.instantiateViewController(withIdentifier: "ProfileControllerNavigationController") as? UINavigationController, let rootViewContoller = profileNavigationController.viewControllers[0] as? ProfileViewController {
                //                rootViewContoller.test = "test String"
                self.present(profileNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 3 {
            if let advancedSearchNavigationController = storyboard?.instantiateViewController(withIdentifier: "AdvancedSearchControllerNavigationController") as? UINavigationController, let rootViewContoller = advancedSearchNavigationController.viewControllers[0] as? AdvancedSearchViewController {
                //                rootViewContoller.test = "test String"
                self.present(advancedSearchNavigationController, animated: true, completion: nil)
            }
        }else if indexPath.row == 4 {
            if let myMessagesNavigationController = storyboard?.instantiateViewController(withIdentifier: "MyMessagesControllerNavigationController") as? UINavigationController, let rootViewContoller = myMessagesNavigationController.viewControllers[0] as? ChatViewController {
                //                rootViewContoller.test = "test String"
                self.present(myMessagesNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 5 {
            if let myAdvertisementsNavigationController = storyboard?.instantiateViewController(withIdentifier: "MyAdvertisementsControllerNavigationController") as? UINavigationController, let rootViewContoller = myAdvertisementsNavigationController.viewControllers[0] as? AdvertisementsViewController {
                rootViewContoller.selected = 1
                self.present(myAdvertisementsNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 6 {
            if let myAdvertisementsNavigationController = storyboard?.instantiateViewController(withIdentifier: "MyAdvertisementsControllerNavigationController") as? UINavigationController, let rootViewContoller = myAdvertisementsNavigationController.viewControllers[0] as? AdvertisementsViewController {
                rootViewContoller.selected = 3
                self.present(myAdvertisementsNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 7 {
            if let myAdvertisementsNavigationController = storyboard?.instantiateViewController(withIdentifier: "MyAdvertisementsControllerNavigationController") as? UINavigationController, let rootViewContoller = myAdvertisementsNavigationController.viewControllers[0] as? AdvertisementsViewController {
                rootViewContoller.selected = 4
                self.present(myAdvertisementsNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 8 {
            if let allAdvertisementsNavigationController = storyboard?.instantiateViewController(withIdentifier: "AllAdvertisementsControllerNavigationController") as? UINavigationController, let rootViewContoller = allAdvertisementsNavigationController.viewControllers[0] as? AllAdvertisementsViewController {
                //                rootViewContoller.test = "test String"
                self.present(allAdvertisementsNavigationController, animated: true, completion: nil)
            }
        } else if indexPath.row == 9 {
            UserDefaultManager.shared.authorization = nil
            if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "SignInNavigationController") as? UINavigationController, let rootViewContoller = signInNavigationController.viewControllers[0] as? SignInViewController {
                
                self.present(signInNavigationController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    
}
