//
//  EditPasswordTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/16/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol EditPasswordTableViewCellDelegate {
    func didPasswordTextFieldEditted(tag: Int,text: String)
}

class EditPasswordTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    
    var delegate: EditPasswordTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUI()
        inputTextField.delegate = self
        inputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        selectionStyle = .none
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        didPasswordTextFieldEditted(tag: textField.tag, text: textField.text!)
    }

    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didPasswordTextFieldEditted(tag: Int,text: String) {
        if let delegateValue = delegate {
            delegateValue.didPasswordTextFieldEditted(tag: tag,text: text)
        }
    }
    
}


extension EditPasswordTableViewCell: UITextFieldDelegate {
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if inputTextField.tag == 0 {
//            didPasswordTextFieldEditted(tag: 0, text: inputTextField.text!)
//        } else if inputTextField.tag == 1 {
//            didPasswordTextFieldEditted(tag: 1, text: inputTextField.text!)
//        } else if inputTextField.tag == 2 {
//            didPasswordTextFieldEditted(tag: 2, text: inputTextField.text!)
//        }
//        return true
//    }
    
//        func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//            if inputTextField.tag == 0 {
//                didPasswordTextFieldEditted(tag: 0, text: inputTextField.text!)
//            } else if inputTextField.tag == 1 {
//                didPasswordTextFieldEditted(tag: 1, text: inputTextField.text!)
//            } else if inputTextField.tag == 2 {
//                didPasswordTextFieldEditted(tag: 2, text: inputTextField.text!)
//            }
//        }
    
}
