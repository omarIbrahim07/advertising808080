//
//  EditPasswordViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/15/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class EditPasswordViewController: BaseViewController {
    
    var oldPassword: String?
    var newPassword: String?
    var confirmNewPassword: String?

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "تعديل رقم المرور"
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "EditPasswordTableViewCell", bundle: nil), forCellReuseIdentifier: "EditPasswordTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func editPassword() {
        
        guard let oldPassword = self.oldPassword, oldPassword.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة المرور القديمة"
            showError(error: apiError)
            return
        }
        
        
        guard let newPassword = self.newPassword, newPassword.count > 0 else {
            let apiError = APIError()
            apiError.message =  "برجاء إدخال كلمة المرور الجديدة"
            showError(error: apiError)
            return
        }
        
        guard let confirmNewPassword = self.confirmNewPassword, confirmNewPassword.count > 0, confirmNewPassword == self.newPassword else {
            let apiError = APIError()
            apiError.message =  "برجاء إدخال كلمة المرور الجديدة صحيحة"
            showError(error: apiError)
            return
        }
        
        print("oldPassword: \(oldPassword)")
        print("newPassword: \(newPassword)")

        
        let parameters = [
            "oldpassword" : oldPassword as AnyObject,
            "newpassword" : newPassword as AnyObject,
            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AuthenticationAPIManager().changePassword(basicDictionary: parameters, onSuccess: { (message) in
            print(message)
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    @IBAction func changePasswordPressed(_ sender: Any) {
        print("Pressed")
        editPassword()
    }
    

}

extension EditPasswordViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            if let cell: EditPasswordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EditPasswordTableViewCell") as? EditPasswordTableViewCell {
                
                cell.headerLabel.text = "كلمة المرور القديمة"
                cell.inputTextField.placeholder = "كلمة المرور القديمة"
                cell.inputTextField.tag = 0
                cell.delegate = self
                
                return cell
            }
        }
        
        else if indexPath.row == 1 {
            if let cell: EditPasswordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EditPasswordTableViewCell") as? EditPasswordTableViewCell {
                
                cell.headerLabel.text = "كلمة المرور الجديدة"
                cell.inputTextField.placeholder = "كلمة المرور الجديدة"
                cell.inputTextField.tag = 1
                cell.delegate = self
                
                return cell
            }
        }
        
        else if indexPath.row == 2 {
            if let cell: EditPasswordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EditPasswordTableViewCell") as? EditPasswordTableViewCell {
                
                cell.headerLabel.text = "تأكيد كلمة المرور"
                cell.inputTextField.placeholder = "تأكيد كلمة المرور"
                cell.inputTextField.tag = 2
                cell.delegate = self
                
                return cell
            }
        }

        return UITableViewCell()
    }
    
}

extension EditPasswordViewController: EditPasswordTableViewCellDelegate {
    func didPasswordTextFieldEditted(tag: Int, text: String) {
        if tag == 0 {
            self.oldPassword = text
            print(self.oldPassword as Any)
        } else if tag == 1 {
            self.newPassword = text
            print(self.newPassword as Any)
        } else if tag == 2 {
            self.confirmNewPassword = text
            print(self.confirmNewPassword as Any)
        }
    }
    
    
}
