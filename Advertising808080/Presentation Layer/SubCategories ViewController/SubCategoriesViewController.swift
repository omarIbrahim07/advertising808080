//
//  SubCategoriesViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/12/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SubCategoriesViewController: BaseViewController {
    
    //MARK:- Variables
    var subCategories = [SubCategories]()
    
    var categoryParentId: String?
    
    var secondaryCategoryID: String?
    
    //MARK:- Outlets
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAdvertisementButton: UIButton!
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureSearchBar()
        configureTableView()
        getSubCategoryBanner()
        getSubCategories()
    }
    
    //MARK:- Configuration UI
    func configureUI() {
        addAdvertisementButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        addAdvertisementButton.addCornerRadius(raduis: addAdvertisementButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureSearchBar() {
        searchBar.placeholder = "ابحث"
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }

    func configureTableView() {
        tableView.register(UINib(nibName: "SubCategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "SubCategoriesTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getSubCategories() {
        
        self.startLoading()
        
        CategoriesAPIManager().getSubCategories(Parent: String(categoryParentId!), basicDictionary: [:], onSuccess: { (subCategories) in
            self.subCategories = subCategories
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getSubCategoryBanner() {
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        CategoriesAPIManager().getMainBanner(basicDictionary: [:], onSuccess: { (imageString) in
            
            let imageURL = "http://new.808080group.com/storage/\(imageString)"
            
            self.bannerImage.loadImageFromUrl(imageUrl: imageURL)
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }
    
    //MARK:- Segue to Secondary Categories
    func goToSecondaryCategories(id: String) {
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "SecondaryCategoriesViewController") as? SecondaryCategoriesViewController {
            viewController.secondaryParentId = id
//            self.navigationController?.show(viewController, sender: self)
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func addAdvertisementButtonPressed(_ sender: Any) {
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 0
            self.present(addNavigationController, animated: true, completion: nil)
        }
    }
    

}

extension SubCategoriesViewController: UISearchBarDelegate {
    
}

//MARK:- TableView
extension SubCategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: SubCategoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesTableViewCell") as? SubCategoriesTableViewCell {
            let subcategoryType = subCategories[indexPath.row]
            cell.subCategoryTitleLabel.text = subcategoryType.name
//            cell.subCategoryAdvertisesNumberLabel.text = String(subcategoryType.postsCount) + " إعلان"
            return cell
        }
        
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        secondaryCategoryID = String(subCategories[indexPath.row].id)
        goToSecondaryCategories(id: secondaryCategoryID!)
    }

}
