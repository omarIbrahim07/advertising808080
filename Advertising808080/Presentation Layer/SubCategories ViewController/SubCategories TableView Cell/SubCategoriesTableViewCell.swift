//
//  SubCategoriesTableViewCell.swift
//  AdForest
//
//  Created by Omar Ibrahim on 3/8/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SubCategoriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var subCategoryTitleLabel: UILabel!
    @IBOutlet weak var subCategoryAdvertisesNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
