//
//  SideMenuViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class SideMenuViewController: BaseViewController {

    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToogleSideMenu"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func toggleSideMenu() {
        if sideMenuOpen == true {
            sideMenuOpen = false
            sideMenuConstraint.constant = -280
        } else {
            sideMenuOpen = true
            sideMenuConstraint.constant = 0
        }
    }


}
