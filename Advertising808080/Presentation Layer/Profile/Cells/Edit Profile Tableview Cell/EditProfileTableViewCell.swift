//
//  AdvertisementOwnerDetailsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/14/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import Cosmos


protocol EditProfileButtonTableViewCellDelegate {
    func didEditProfilePressedButton(choosed: String)
}

class EditProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var outView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var starEvaluationLabel: UILabel!
    @IBOutlet weak var EditProfileStack: UIStackView!
    @IBOutlet weak var editLabel: UILabel!
    
    var option: Bool = true
    
    var delegate: EditProfileButtonTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUI()
        selectionStyle = .none
    }
    
    func configureUI() {
        outView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
        userImage.addCornerRadius(raduis: userImage.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    //MARK:- Delegate Helpers
    func didEditProfilePressedButton(choosed: String) {
        if let delegateValue = delegate {
            delegateValue.didEditProfilePressedButton(choosed: choosed)
        }
    }
    
    @IBAction func editProfilePressed(_ sender: Any) {
        print("Edit is pressed")
        if option == true {
            didEditProfilePressedButton(choosed: "EditProfile")
        } else if option == false {
            didEditProfilePressedButton(choosed: "EditPassword")
        }
    }
    
}
