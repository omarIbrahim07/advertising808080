//
//  MyProfileTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/21/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class MyProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
//    var viewModell: User! {
//        didSet{
//            bindData()
//        }
//    }
//    
//    func bindData() {
//        
//        mainLabel.text = viewModell.name
//        descriptionLabel.text = viewModell.lastLogInAt
//        
//        //        let url: URL = URL(string: viewModell.imageURL)!
//        //        comicsImage.kf.setImage(with: url)
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
