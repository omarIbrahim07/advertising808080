//
//  ProfileViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/21/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAdvertisementButton: UIButton!
    
    var userCurrent = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureTableView()
        updateProfile()
        // Do any additional setup after loading the view.
    }
    
    func configureUI() {
        addAdvertisementButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        addAdvertisementButton.addCornerRadius(raduis: addAdvertisementButton.frame.height / 2 , borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    func configureTableView() {
        tableView.register(UINib(nibName: "EditProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProfileTableViewCell")
        tableView.register(UINib(nibName: "MyProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "MyProfileTableViewCell")
        tableView.register(UINib(nibName: "AdvertisementTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementTypeTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func updateProfile() {
        
        weak var weakSelf = self
        
        startLoading()
        AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
            
            weakSelf?.stopLoadingWithSuccess()
            
            self.userCurrent = user
            
            self.tableView.reloadData()

            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func presentEditProfile() {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController")
//        let naviagationController = UINavigationController(rootViewController: viewController)
//        appDelegate.window!.rootViewController = naviagationController
//        appDelegate.window!.makeKeyAndVisible()
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
    @IBAction func addAdvertisementButtonPressed(_ sender: Any) {
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 0
            self.present(addNavigationController, animated: true, completion: nil)
        }
    }
    

}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: EditProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTableViewCell") as? EditProfileTableViewCell {
                cell.delegate = self
                if let userName = userCurrent.name {
                    cell.userNameLabel.text = userName
                }
                
                if let userImage = userCurrent.photo {
                    let imageURL = "http://new.808080group.com/storage/\(userImage)"
                    cell.userImage.loadImageFromUrl(imageUrl: imageURL)
                }
                
                if let userLastLogIn = userCurrent.lastLogInAt {
                    cell.hoursLabel.text = userLastLogIn
                }
                cell.option = true
                
                return cell
            }
        }
            
        else if indexPath.row == 1 {
            if let cell: AdvertisementTypeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementTypeTableViewCell") as? AdvertisementTypeTableViewCell {
                return cell
            }
        }
        
        else if indexPath.row == 2 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "الإسم"
                
                cell.descriptionLabel.text = "لا يوجد"
                if let userName = userCurrent.name {
                    cell.descriptionLabel.text = userName
                }
                return cell
            }
        }
        
        else if indexPath.row == 3 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "البريد الإلكتروني"
                
                cell.descriptionLabel.text = "لا يوجد"
                if let userEmail = userCurrent.email {
                    cell.descriptionLabel.text = userEmail
                }
                
                return cell
            }
        }
        
        else if indexPath.row == 4 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "رقم الهاتف"
                
                cell.descriptionLabel.text = "لا يوجد"
                if let userPhone = userCurrent.phone {
                    cell.descriptionLabel.text = userPhone
                }
                
                return cell
            }
        }
            
        else if indexPath.row == 5 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "نوع الحساب"
                
                cell.descriptionLabel.text = "لا يوجد"
//                if let userAccountType = userCurrent.phone {
//                    cell.descriptionLabel.text = "فردي"
//                }
                
                return cell
            }
        }
        
        else if indexPath.row == 6 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "الموقع"
                
                cell.descriptionLabel.text = "لا يوجد"
                if let userCountry = userCurrent.country {
                    cell.descriptionLabel.text = userCountry
                }
                
                return cell
            }
        }
        
        else if indexPath.row == 7 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "عدد الإعلانات المجانية"
                
                cell.descriptionLabel.text = "لا يوجد"
                
                return cell
            }
        }
        
        else if indexPath.row == 8 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "عدد الإعلانات المميزة"
                cell.descriptionLabel.text = "لا يوجد"
                return cell
            }
        }
        
        else if indexPath.row == 9 {
            if let cell: MyProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as? MyProfileTableViewCell {
                cell.mainLabel.text = "عدد الإعلانات المباعة"
                cell.descriptionLabel.text = "لا يوجد"
                return cell
            }
        }
        
        
        return UITableViewCell()
    }
    
    
}

extension ProfileViewController: EditProfileButtonTableViewCellDelegate {
    
    func didEditProfilePressedButton(choosed: String) {
        if choosed == "EditProfile" {
            self.presentEditProfile()
        }
    }
}
