//
//  SecondLevelViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/24/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class SecondLevelViewController: CommonExampleController {

    var postID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "إضافة صور"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    //MARK:- Configurations
    func configureNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "حفظ", style: .plain, target: self, action: #selector(saveAction))
    }
    
    //MARK:- API Calls
    func saveAdPictures() {

        guard let postIDValue = postID else {
            return
        }
        
        let parameters: [String : AnyObject] = [
            "postID" : postIDValue as AnyObject
        ]
        
        var imageDataArr = [Data]()
        for asset in assets {
            let assetImage = getAssetThumbnail(asset: asset)
            let imgData = assetImage.jpegData(compressionQuality: 1)
            imageDataArr.append(imgData!)
        }
        
        startLoading()
        weak var weakSelf = self
        AdvertisementsAPIManager().saveAdPictures(imageDataArray: imageDataArr, basicDictionary: parameters, onSuccess: {
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.dismiss(animated: true, completion: nil)
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    //MARK:- Actions
    @objc fileprivate func saveAction() {
        saveAdPictures()
    }

    override func pressedPick(_ sender: Any) {
        let picker = AssetsPickerViewController()
        picker.pickerDelegate = self
        present(picker, animated: true, completion: nil)
    }
    
    //MARK:- Helpers
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        self.view.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
    }
    
    func startLoading() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        HUD.show(.progress)
    }
    
    func stopLoadingWithSuccess() {
        HUD.hide()
    }
    
    func stopLoadingWithError(error: APIError) {
        HUD.hide()
        showError(error: error)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }

}
