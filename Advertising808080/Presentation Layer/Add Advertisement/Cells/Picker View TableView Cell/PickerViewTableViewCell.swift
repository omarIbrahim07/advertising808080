//
//  PickerViewTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol CategoriesPickerViewTableViewCellDelegate {
    func didSelectPickerView(tag: Int,id: Int)
}

class PickerViewTableViewCell: UITableViewCell {
    
    var cellSelected: String?
    
    var id: Int?
    var selectedInt: Int?

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var advertisementSubSpecification: String?
    var advertisementCost: String?
    
    var mainCategoriesTitles: [String] = []
    var mainCategoriesIndexs : [Int] = []
    var subCategoriesTitles: [String] = []
    var subCategoriesIndexs : [Int] = []
    
    var cities: [City] = []
    var citiesName: [String] = []
    var citiesId: [Int] = []
    
    var postTypes: [PostType] = []
    var postTypesNames: [String] = []
    var postTypesId: [Int] = []
    
    var delegate: CategoriesPickerViewTableViewCellDelegate?
    
    var viewModelsMainCategories : [MainCategories] = []{
        didSet {
            getMainCategories()
        }
    }
    
    var viewModelsSubCategories : [SubCategories] = []{
        didSet {
            getSubCategories()
        }
    }
    
    func getMainCategories() {
        
        for category in self.viewModelsMainCategories {
            self.mainCategoriesTitles.append(category.name)
            self.mainCategoriesIndexs.append(category.id)
        }
        
//        for n in 0..<self.mainCategories.count {
//            self.mainCategoriesTitles.append(self.mainCategories[n].name)
//            self.mainCategoriesIndexs.append(self.mainCategories[n].id)
//        }
    }
    
    func getSubCategories() {
        
        subCategoriesTitles = []
        subCategoriesIndexs = []
        for subCategory in self.viewModelsSubCategories {
            self.subCategoriesTitles.append(subCategory.name)
            self.subCategoriesIndexs.append(subCategory.id)
        }
    }
    
    func getCities() {
        
        guard let userCountry = UserDefaultManager.shared.currentUser?.countryCode else {
            return
        }
        
        CategoriesAPIManager().getCities(id: userCountry, basicDictionary: [:], onSuccess: { (cities) in
                self.cities = cities
                
                for city in self.cities {
                    self.citiesName.append(city.name)
                    self.citiesId.append(city.id)
                }
            
            }) { (error) in
                print("error")
            }
    }
    
    func getPostTypes() {
        
        CategoriesAPIManager().getPostTypes(basicDictionary: [:], onSuccess: { (postTypes) in
            self.postTypes = postTypes
            
            for postType in self.postTypes {
                self.postTypesNames.append(postType.name)
                self.postTypesId.append(postType.id)
            }
            
        }) { (error) in
            print("error")
        }
        
    }
    
    let option = [
        "نعم",
        "لا"
    ]
    
    let mathOption = [
        1,
        0
    ]
    
    var Selected: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        configureTextFields()
        createDayPicker()
        getCities()
        getPostTypes()
        selectionStyle = .none
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
    
    func configureTextFields() {
        configureTextField(textField: textField, imageName: "icons8-new-post-50")
    }
    
    func configureTextField(textField: UITextField, imageName: String) {
        textField.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 189, y: 0, width: 20, height: 20))
        let textFieldImage = UIImage(named: imageName)
        imageView.image = textFieldImage
        textField.rightView = imageView
    }
    
    func createDayPicker() {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        textField.inputView = Picker
    }
    

    
    //MARK:- Delegate Helpers
    func didSelectPickerView(tag: Int,id: Int) {
        if let delegateValue = delegate {
            delegateValue.didSelectPickerView(tag: tag,id: id)
        }
    }
    
    
}


extension PickerViewTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if textField.tag == 0 {
             return mainCategoriesTitles.count
        } else if textField.tag == 1 {
            return subCategoriesTitles.count
        }else if textField.tag == 2 {
            return option.count
        } else if textField.tag == 3 {
            return postTypesNames.count
        } else if textField.tag == 4 {
            return citiesName.count
        }
//        } else if textField.tag == 5 {
//            return insurance.count
//        } else if textField.tag == 6 {
//            return advertisementType.count
//        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if textField.tag == 0 {
            return mainCategoriesTitles[row]
        } else if textField.tag == 1 {
            return subCategoriesTitles[row]
        }else if textField.tag == 2 {
            return option[row]
        } else if textField.tag == 3 {
//            return currency[row]
            return postTypesNames[row]
        } else if textField.tag == 4 {
            return citiesName[row]
        }
//        } else if textField.tag == 5 {
//            return insurance[row]
//        } else if textField.tag == 6 {
//            return advertisementType[row]
//        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textField.tag == 0 {
            
            Selected = mainCategoriesTitles[row]
            textField.text = Selected
            didSelectPickerView(tag: 0,id: mainCategoriesIndexs[row])
//            id = mainCategoriesIndexs[row]
            self.view.endEditing(true)
        } else if textField.tag == 1 {
            
            Selected = subCategoriesTitles[row]
            textField.text = Selected
            didSelectPickerView(tag: 1,id: subCategoriesIndexs[row])
            self.view.endEditing(true)
//            advertisementSubSpecification = Selected
            
        } else if textField.tag == 2 {
            
            Selected = option[row]
            textField.text = Selected
            didSelectPickerView(tag: 2,id: mathOption[row])
            self.view.endEditing(true)
        }
        
        else if textField.tag == 3 {
            
            Selected = postTypesNames[row]
            textField.text = Selected
            didSelectPickerView(tag: 3,id: postTypesId[row])
            self.view.endEditing(true)
        } else if textField.tag == 4 {
            
            Selected = citiesName[row]
            textField.text = Selected
            didSelectPickerView(tag: 4,id: citiesId[row])
            self.view.endEditing(true)
        }
//        else if textField.tag == 5 {
//
//            Selected = insurance[row]
//            textField.text = Selected
//            self.view.endEditing(true)
//        } else if textField.tag == 6 {
//
//            Selected = advertisementType[row]
//            textField.text = Selected
//            self.view.endEditing(true)
//        }
    }
    
    
    
    
}
