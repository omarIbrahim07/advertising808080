//
//  TextFieldTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol TextFieldTableViewCellDelegate {
    func didTextFieldEditted(tag: Int,text: String)
}

class TextFieldTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    
    
    var delegate: TextFieldTableViewCellDelegate?
    var textEntered: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inputTextField.delegate = self
        configureUI()
        inputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        selectionStyle = .none
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        didTextFieldEditted(tag: textField.tag, text: textField.text!)
    }

    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), borderWidth: 1)
    }
    
    //MARK:- Delegate Helpers
    func didTextFieldEditted(tag: Int,text: String) {
        if let delegateValue = delegate {
            delegateValue.didTextFieldEditted(tag: tag,text: text)
        }
    }
    
}

extension TextFieldTableViewCell: UITextFieldDelegate {
    
    
    
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        if inputTextField.tag == 7 {
//            didTextFieldEditted(tag: 7, text: inputTextField.text!)
//        } else if inputTextField.tag == 8 {
//            didTextFieldEditted(tag: 8, text: inputTextField.text!)
//        } else if inputTextField.tag == 9 {
//            didTextFieldEditted(tag: 9, text: inputTextField.text!)
//        }
//    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if inputTextField.tag == 5 {
//            didTextFieldEditted(tag: 5, text: inputTextField.text!)
//            return true
//        } else if inputTextField.tag == 6 {
//            didTextFieldEditted(tag: 6, text: inputTextField.text!)
//            return true
//        } else if inputTextField.tag == 7 {
//            didTextFieldEditted(tag: 7, text: inputTextField.text!)
//            return true
//        }
//        return true
//    }
    
}

