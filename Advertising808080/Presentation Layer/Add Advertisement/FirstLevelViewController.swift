//
//  FirstLevelViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/20/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class FirstLevelViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var mainCategories: [MainCategories] = []
    var subCategories: [SubCategories] = []
    
    var advertisementDetails: AdvertisementDetailsModel?
    
    var categoryID : Int?
    var subCategoryId : Int?
    var negotiable: Int?
    var cityId: Int?
    var advertisementTypeId: Int?
    
    var isEdited: Int?
    var editedAdvertisementId : Int?
    
    var advertisementTitle: String?
    var advertisementPrice: String?
    var advertisementDescription: String?
    
    var mainCategoriesTitles : [String] = []
    var mainCategoriesIndexs : [Int] = []
    
    var test: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getMainCategories()
        
        if isEdited == 1 {
            getAdvertisementDetails()
        }
        
        // Do any additional setup after loading the view.
    }
    

    func configureTableView() {
        tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        tableView.register(UINib(nibName: "PickerViewTableViewCell", bundle: nil), forCellReuseIdentifier: "PickerViewTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getMainCategories() {
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        CategoriesAPIManager().getMainCategories(basicDictionary: [:], onSuccess: { (Categories) in
            self.mainCategories = Categories
            
//            for n in 0..<Categories.count {
//                self.mainCategoriesTitles.append(Categories[n].name)
//                self.mainCategoriesIndexs.append(Categories[n].id)
//            }
            
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getSubCategories() {
        
        self.startLoading()
        
        CategoriesAPIManager().getSubCategories(Parent: String(self.categoryID!), basicDictionary: [:], onSuccess: { (subCategories) in
            self.subCategories = subCategories

            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func getAdvertisementDetails() {

        self.startLoading()

        CategoriesAPIManager().getAdvertisementDetailsByAdId(advertisementId: self.editedAdvertisementId!, basicDictionary: [:], onSuccess: { (advertisementDetails) in

            self.advertisementDetails = advertisementDetails

            self.stopLoadingWithSuccess()
            
            self.tableView.reloadData()

        }) { (error) in
            print("error")
        }
    }
    
    func editFirstLevelAdvertisement() {
        
        guard let advertisementTitle = self.advertisementTitle, advertisementTitle.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان الإعلان"
            showError(error: apiError)
            return
        }
        
        
        guard let advertisementCategoryId = self.subCategoryId, advertisementCategoryId > 0 else {
            let apiError = APIError()
            apiError.message =  "برجاء إدخال الصنف الفرعي"
            showError(error: apiError)
            return
        }
        
        guard let advertisementDescription = self.advertisementDescription, advertisementDescription.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال وصف الإعلان"
            showError(error: apiError)
            return
        }
        
        //        guard let advertisementPrice = self.advertisementPrice, advertisementPrice.count > 0 else {
        //            let apiError = APIError()
        //            apiError.message = "برجاء إدخال سعر الإعلان"
        //            showError(error: apiError)
        //            return
        //        }
        
        guard let advertisementPrice = self.advertisementPrice else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال سعر الإعلان"
            showError(error: apiError)
            return
        }
        
        guard let advertisementCityId = self.cityId, advertisementCityId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المدينة"
            showError(error: apiError)
            return
        }
        
        guard let negotiable = self.negotiable, negotiable == 0 || negotiable == 1 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال قابل للتفاوض أم لا"
            showError(error: apiError)
            return
        }
        
        guard let advertisementTypeId = self.advertisementTypeId, advertisementTypeId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال نوع الإعلان"
            showError(error: apiError)
            return
        }
        
        //        print("advertisementTitle: \(advertisementTitle)")
        //        print("advertisementCategoryId: \(advertisementCategoryId)")
        //        print("advertisementDescription: \(advertisementDescription)")
        //        print("advertisementPrice: \(advertisementPrice)")
        //        print("advertisementCityId: \(advertisementCityId)")
        
        let parameters = [
            "title" : advertisementTitle as AnyObject,
            "category_id" : advertisementCategoryId as AnyObject,
            "description" : advertisementDescription as AnyObject,
            "price" : advertisementPrice as AnyObject,
            "city_id" : advertisementCityId as AnyObject,
            "negotiable" : negotiable as AnyObject,
            "post_type_id" : advertisementTypeId as AnyObject
        ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AdvertisementsAPIManager().editFirstLevelPostAdvertisement(postId: self.editedAdvertisementId! ,basicDictionary: parameters, onSuccess: { (postID) in
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentSecondLevel(withPostID: postID)
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func postFirstLevelAdvertisement() {
        
        guard let advertisementTitle = self.advertisementTitle, advertisementTitle.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان الإعلان"
            showError(error: apiError)
            return
        }

        
        guard let advertisementCategoryId = self.subCategoryId, advertisementCategoryId > 0 else {
            let apiError = APIError()
            apiError.message =  "برجاء إدخال الصنف الفرعي"
            showError(error: apiError)
            return
        }
        
        guard let advertisementDescription = self.advertisementDescription, advertisementDescription.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال وصف الإعلان"
            showError(error: apiError)
            return
        }
        
//        guard let advertisementPrice = self.advertisementPrice, advertisementPrice.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال سعر الإعلان"
//            showError(error: apiError)
//            return
//        }
        
        guard let advertisementPrice = self.advertisementPrice, advertisementPrice.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال سعر الإعلان"
            showError(error: apiError)
            return
        }
        
        guard let advertisementCityId = self.cityId, advertisementCityId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المدينة"
            showError(error: apiError)
            return
        }
        
        guard let negotiable = self.negotiable, negotiable == 0 || negotiable == 1 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال قابل للتفاوض أم لا"
            showError(error: apiError)
            return
        }
        
        guard let advertisementTypeId = self.advertisementTypeId, advertisementTypeId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال نوع الإعلان"
            showError(error: apiError)
            return
        }
        
//        print("advertisementTitle: \(advertisementTitle)")
//        print("advertisementCategoryId: \(advertisementCategoryId)")
//        print("advertisementDescription: \(advertisementDescription)")
//        print("advertisementPrice: \(advertisementPrice)")
//        print("advertisementCityId: \(advertisementCityId)")
        
        let parameters = [
            "title" : advertisementTitle as AnyObject,
            "category_id" : advertisementCategoryId as AnyObject,
            "description" : advertisementDescription as AnyObject,
            "price" : advertisementPrice as AnyObject,
            "city_id" : advertisementCityId as AnyObject,
            "negotiable" : negotiable as AnyObject,
            "post_type_id" : advertisementTypeId as AnyObject
            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AdvertisementsAPIManager().firstLevelPostAdvertisement(basicDictionary: parameters, onSuccess: { (postID) in
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentSecondLevel(withPostID: postID)
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func presentSecondLevel(withPostID postID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SecondLevelViewController") as! SecondLevelViewController
        viewController.postID = postID
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func secondLevelAddAdvertisementButtonPressed(_ sender: Any) {
        if isEdited == 0 {
            postFirstLevelAdvertisement()
        } else if isEdited == 1 {
            editFirstLevelAdvertisement()
        }
    }
    

}

extension FirstLevelViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "التصنيفات"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 0
                cell.viewModelsMainCategories = self.mainCategories
                
//                if let category = self.advertisementDetails?.categoryId {
//                    cell.textField.text = category
//                }
                
                return cell
            }
        }
            
        else if indexPath.row == 1 {
            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "التصنيف الفرعي"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 1
                cell.viewModelsSubCategories = self.subCategories
                
//                if let subCat = self.advertisementDetails? {
//                    cell.textField.text = negotiable
//                }
//                advertisement?.categoryId = cell.advertisementSubSpecification
                
                return cell
            }
        }
            
        
        else if indexPath.row == 2 {
            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "السعر قابل للتفاوض"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 2
                
//                if let negotiable = self.advertisementDetails?.negotiable {
//                    cell.textField.text = negotiable
//                }
                
                return cell
            }
        }
        
//        else if indexPath.row == 3 {
//            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
//                cell.headerLabel.text = "العملة الإعلانية"
//                cell.textField.placeholder = "الرجاء الاختيار"
//                cell.textField.tag = 3
//                return cell
//            }
//        }
        
//        else if indexPath.row == 4 {
//            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
//                cell.headerLabel.text = "الحالة"
//                cell.textField.placeholder = "الرجاء الاختيار"
//                cell.textField.tag = 4
//                return cell
//            }
//        }
        
//        else if indexPath.row == 5 {
//            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
//                cell.headerLabel.text = "الضمان"
//                cell.textField.placeholder = "الرجاء الاختيار"
//                cell.textField.tag = 5
//                return cell
//            }
//        }
        
        else if indexPath.row == 3 {
            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "نوع الإعلان"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 3
                
//                if let advertisementType = self.advertisementDetails?. {
//                    cell.textField.text = city
//                }
                
                return cell
            }
        }
        
        else if indexPath.row == 4 {
            if let cell: PickerViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PickerViewTableViewCell") as? PickerViewTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "المدينة"
                cell.textField.placeholder = "الرجاء الاختيار"
                cell.textField.tag = 4
                
//                if let city = self.advertisementDetails?.city {
//                    cell.textField.text = city
//                }
                
                return cell
            }
        }
            
        if indexPath.row == 5 {
            if let cell: TextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as? TextFieldTableViewCell {
                cell.delegate = self
                cell.headerLabel.text = "عنوان الإعلان"
                cell.inputTextField.placeholder = "العنوان"
                cell.inputTextField.tag = 5
                
//                if let title = self.advertisementDetails?.title {
//                    cell.inputTextField.text = title
//                }
                
                return cell
            }
        }
        
        else if indexPath.row == 6 {
            if let cell: TextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as? TextFieldTableViewCell {
                cell.headerLabel.text = "سعر الإعلان"
                cell.inputTextField.placeholder = "سعر الإعلان"
                cell.delegate = self
                cell.inputTextField.tag = 6
                
//                if let price = self.advertisementDetails?.price {
//                    cell.inputTextField.text = price
//                }
                
                return cell
            }
        }
        
        else if indexPath.row == 7 {
            if let cell: TextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as? TextFieldTableViewCell {
                cell.headerLabel.text = "وصف الإعلان"
                cell.inputTextField.placeholder = "الوصف"
                cell.delegate = self
                cell.inputTextField.tag = 7
                
//                if let description = self.advertisementDetails?.description {
//                    cell.inputTextField.text = description
//                }
                
                return cell
            }
        }
        
//        else if indexPath.row == 10 {
//            if let cell: TextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell") as? TextFieldTableViewCell {
//                cell.headerLabel.text = "رابط اليوتيوب"
//                cell.inputTextField.placeholder = "ادخل الرابط"
//                cell.delegate = self
//                cell.inputTextField.tag = 10
//
//                return cell
//            }
//        }
        
        
        
        return UITableViewCell()
    }
    
    
    
}

extension FirstLevelViewController: CategoriesPickerViewTableViewCellDelegate {
    func didSelectPickerView(tag: Int,id: Int) {
        if tag == 0 {
            self.categoryID = id
            getSubCategories()
            print(self.categoryID as Any)
        } else if tag == 1 {
            self.subCategoryId = id
            tableView.reloadData()
            print(self.subCategoryId as Any)
        } else if tag == 2 {
            self.negotiable = id
            print(self.negotiable as Any)
        } else if tag == 3 {
            self.advertisementTypeId = id
            print(self.advertisementTypeId as Any)
        } else if tag == 4 {
            self.cityId = id
            print(self.cityId as Any)
        }
    }
}

extension FirstLevelViewController: TextFieldTableViewCellDelegate {
    func didTextFieldEditted(tag: Int, text: String) {
        if tag == 5 {
            self.advertisementTitle = text
            print(self.advertisementTitle as Any)
        } else if tag == 6 {
            self.advertisementPrice = text
            print(self.advertisementPrice as Any)
        } else if tag == 7 {
            self.advertisementDescription = text
            print(self.advertisementDescription as Any)
        }
    }


}
