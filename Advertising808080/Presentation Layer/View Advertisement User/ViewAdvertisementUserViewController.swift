//
//  ViewAdvertisementUserViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/17/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class ViewAdvertisementUserViewController: BaseViewController {
    
    var userId: String?
    
    var userName: String?
    
    var userAdvertisements: [AdvertisementDetailsModel] = []

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "الملف الشخصي للمعلِن"
        configureTableView()
        getUserAdvertisements()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "AdvertisementOwnerDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "AdvertisementOwnerDetailsTableViewCell")
        tableView.register(UINib(nibName: "UserAdvertisementsTableViewCell", bundle: nil), forCellReuseIdentifier: "UserAdvertisementsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    //MARK:- API Calls
    func getUserAdvertisements() {
        
        guard let userID = self.userId else {
            return
        }
        
        self.startLoading()
        
        CategoriesAPIManager().getUserAdvertisements(userID: userID, basicDictionary: [:], onSuccess: { (userAdvertisements) in
            
            self.userAdvertisements = userAdvertisements
            
            self.userName = userAdvertisements[0].contactName
            
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }

}

extension ViewAdvertisementUserViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: AdvertisementOwnerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementOwnerDetailsTableViewCell") as? AdvertisementOwnerDetailsTableViewCell {
//                cell.starEvaluationLabel.isHidden = true
//                cell.starView.isHidden = true
                cell.cellChoosedButton.isHidden = true
                
                cell.userNameLabel.text = "لا يوجد"
                if let userName = self.userName {
                    cell.userNameLabel.text = userName
                }
                
//                cell.hoursLabel.text = "لا يوجد آخر تسجيل دخول"
//                if let createdAt = advertisementDetails?.createdAt {
//                    cell.hoursLabel.text = createdAt
//                }
                
//                if let userImage = advertisementDetails.userImage {
//                    cell.userImage.loadImageFromUrl(imageUrl: "http://new.808080group.com/storage/\(userImage)")
//                }
                
                return cell
            }
        }
        
        else if indexPath.row == 1 {
            if let cell: UserAdvertisementsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserAdvertisementsTableViewCell") as? UserAdvertisementsTableViewCell {
                
                cell.delegate = self
                cell.viewModels = self.userAdvertisements
                
                if let userName = self.userName {
                    cell.headerLabel.text = "إعلانات \(userName)"
                }
                
                
                return cell
            }
        }
        
        
        return UITableViewCell()
    }
    
}

extension ViewAdvertisementUserViewController: UserAdvertisementsTableViewCellDelegate {
    
    func didSelectUserAdvertisementsCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
}
