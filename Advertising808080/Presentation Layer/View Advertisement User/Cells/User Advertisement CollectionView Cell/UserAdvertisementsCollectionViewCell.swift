//
//  UserAdvertisementsCollectionViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/17/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class UserAdvertisementsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var advertisementImage: UIImageView!
    @IBOutlet weak var advertisementTitleLabel: UILabel!
    @IBOutlet weak var advertisementPriceLabel: UILabel!
    @IBOutlet weak var advertisementPlaceLabel: UILabel!
    
    
    var viewModell: AdvertisementDetailsModel! {
        didSet{
            bindData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
        // Initialization code
    }
    
    func configureUI() {
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), borderWidth: 1)
    }
    
    func bindData() {
        
        advertisementTitleLabel.text = "لا يوجد عنوان للإعلان"
        
        if let title = viewModell.title {
            advertisementTitleLabel.text = title
        }
        
        advertisementPlaceLabel.text = "لا يوجد مكان"
        //        if let place = viewModell.place {
        //            advertisementPlaceLabel.text = place
        //        }
        
        advertisementPriceLabel.text = "لا يوجد سعر للإعلان"
        
        if let price = viewModell.price {
            advertisementPriceLabel.text = price
        }
        
        
        
        if let advertisementImage = viewModell.advertisementImage {
            let url: URL = URL(string: "http://new.808080group.com/storage/\(advertisementImage)")!
            self.advertisementImage.kf.setImage(with: url)
        }
        
        
    }

}
