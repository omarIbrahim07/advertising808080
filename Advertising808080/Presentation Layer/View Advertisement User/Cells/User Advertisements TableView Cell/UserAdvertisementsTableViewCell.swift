//
//  UserAdvertisementsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/17/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol UserAdvertisementsTableViewCellDelegate {
    func didSelectUserAdvertisementsCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

class UserAdvertisementsTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: UserAdvertisementsTableViewCellDelegate?
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionView()
        // Initialization code
        selectionStyle = .none
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "UserAdvertisementsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserAdvertisementsCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }

    //MARK:- Delegate Helpers
    func didSelectUserAdvertisementsCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectUserAdvertisementsCollectionViewCell(viewModel: viewModel)
        }
    }
    
}

extension UserAdvertisementsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell: UserAdvertisementsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserAdvertisementsCollectionViewCell", for: indexPath) as? UserAdvertisementsCollectionViewCell {
            
            cell.viewModell = viewModels[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectUserAdvertisementsCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
    
    
}
