//
//  ChatViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/22/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class ChatViewController: BaseViewController {
    
//    var no: Int? = 2
    
    var messages: [Message] = []

    var messageId : Int?
    var postId : String?
    var toUserId : String?
    var parentId : String?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableViewCells()
        getMessages()
        // Do any additional setup after loading the view.
    }
    
    func configureTableViewCells() {
        tableView.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getMessages() {
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        ChatAPIManager().getMessages(basicDictionary: [:], onSuccess: { (messages) in
            self.messages = messages
         
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }
    

    @IBAction func onRefreshButton(_ sender: Any) {
        print("Refresh is pressed")
//        self.view.layoutIfNeeded()
//        no = 3
        self.viewDidLoad()
        self.viewWillAppear(true)
    }
    
    func goToChat() {
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ChatRoomViewController") as? ChatRoomViewController {
            viewController.messageId = self.messageId
            viewController.advertisementId = self.postId
            viewController.toUserId = self.toUserId
            viewController.parentId = self.parentId
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: ChatTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell") as? ChatTableViewCell {
            
            if let messageUser = self.messages[indexPath.row].fromName {
                cell.userNameLabel.text = messageUser
            }
            
            if let messageCreatedAt = self.messages[indexPath.row].createdAt {
                cell.lastMessageTimeLabel.text = messageCreatedAt
            }
            
            if let message = self.messages[indexPath.row].message {
                cell.lastMessageLabel.text = message
            }
            
//            if let senderImage = self.messages[indexPath.row].image {
//                let imageURL = "http://new.808080group.com/storage/"+String(senderImage)
//                cell.userImage.loadImageFromUrl(imageUrl: imageURL)
//            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let parentId = self.messages[indexPath.row].parentId {
            self.parentId = String(parentId)
        }
        
        if let castedParentId = Int(self.parentId!) {
            if castedParentId == 0 {
                self.messageId = self.messages[indexPath.row].id
            } else {
                if let newmessageId = Int(self.messages[indexPath.row].parentId!) {
                    self.messageId = newmessageId
                }
            }
        }
        
        if let newParent = self.messageId {
            self.parentId = String(newParent)
        }
        
        self.postId = self.messages[indexPath.row].postId
        
        self.toUserId = self.messages[indexPath.row].toUserId
        if UserDefaultManager.shared.currentUser?.userID == Int(self.toUserId!) {
            self.toUserId = self.messages[indexPath.row].fromUserId
        } else {
            self.toUserId = self.messages[indexPath.row].toUserId
        }
        
        goToChat()
    }
    
    
}
