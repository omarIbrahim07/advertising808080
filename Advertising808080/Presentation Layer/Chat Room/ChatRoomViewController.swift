//
//  ChatRoomViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/22/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class ChatRoomViewController: BaseViewController {
    
    var advertisementId: String?
    var toUserId: String?
    var message: String?
    var parentId: String?
    
    var chatMessagesParentId: String?
    
    var firstTime = 0
    
    var messageInfo: ReturnedMessage?
    
    var messageId: Int?
    var baseMessageId: Int?
    
    var fromUserID : String?
    var castedFromUserId : Int?
    let userID = UserDefaultManager.shared.currentUser?.userID
    
    var firstMessage: Message?
    var chatMessages: [Message] = []
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    
    let textMessages = [
        "Here's my very first message",
        "I'm going to message another long message that will word wrap",
        "I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap"
    ]
    
    let cellId = "id123"
    let OthercellId = "Other"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "الرسائل"
        messageTextField.delegate = self
        getFirstMessage()
        getChatMessages()
        configureTableViewCells()
        // Do any additional setup after loading the view.
    }
    

@objc func textFieldDidChange(_ textField: UITextField) {
    self.textChatMessage()
}
    
    func configureTableViewCells() {
//        tableView.register(UINib(nibName: "MyMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "MyMessageTableViewCell")
//         tableView.register(UINib(nibName: "OtherMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherMessageTableViewCell")
        tableView.register(MyMessagesTableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.register(OtherMessagesTableViewCell.self, forCellReuseIdentifier: OthercellId)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getFirstMessage() {
        
        guard let messageId = self.messageId, messageId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال نوع الإعلان"
            showError(error: apiError)
            return
        }
        
        let parameters = [
            "messageID" : messageId as AnyObject
        ]
        
        print(messageId)
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        ChatAPIManager().getFirstMessagesChat(basicDictionary: parameters, onSuccess: { (firstMessage) in
            self.firstMessage = firstMessage
            
            self.fromUserID = self.firstMessage?.fromUserId
            self.castedFromUserId = Int(self.fromUserID!)!
                        
            print("7obby")
            
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }
    
    func getChatMessages() {
        
        guard let messageId = self.messageId, messageId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال نوع الإعلان"
            showError(error: apiError)
            return
        }
        
        let parameters = [
            "messageID" : messageId as AnyObject
        ]
        
        print(messageId)
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        ChatAPIManager().getChatMessages(basicDictionary: parameters, onSuccess: { (chatMessages) in
            self.chatMessages = chatMessages
            
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
    }
    
    func textChatMessage() {
        
        
        guard let advertisementId = self.advertisementId, advertisementId.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الإعلان"
            showError(error: apiError)
            return
        }
        
        guard let toUserId = self.toUserId, toUserId.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المرسَل إليه"
            showError(error: apiError)
            return
        }
        
        guard let message = messageTextField.text, message.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رسالتك"
            showError(error: apiError)
            return
        }
        
//        guard let parentId = self.parentId, parentId.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال رسالتك"
//            showError(error: apiError)
//            return
//        }
        
        let parameters = [
            "postID" : advertisementId as AnyObject,
            "to_user_id" : toUserId as AnyObject,
            "message" : message as AnyObject,
            "parent_id" : self.parentId as AnyObject
        ]
        
        print(advertisementId as Any)
        print(toUserId)
        print(message)
        print(parentId)
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        
        ChatAPIManager().textChatMessage(basicDictionary: parameters, onSuccess: { (returnedMessageInfo) in

            self.messageInfo = returnedMessageInfo
            
            if self.firstMessage?.message?.count == nil {
                self.messageId = self.messageInfo?.MessageID
                self.parentId = "\(self.messageInfo?.MessageID)"
            }

            self.getFirstMessage()
            self.getChatMessages()
            
            self.tableView.reloadData()
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
        
        
    }

    @IBAction func didTypeMessage(_ sender: Any) {
        textChatMessage()
        messageTextField.text = ""
    }
}

extension ChatRoomViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if firstMessage?.message?.count == nil {
            return 0
        } else {
            return self.chatMessages.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            if self.userID == self.castedFromUserId {
                if let cell: MyMessagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? MyMessagesTableViewCell {
                    cell.messageLabel.text = self.firstMessage?.message
                    
                    return cell
                }
            } else {
                if let cell: OtherMessagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: OthercellId, for: indexPath) as? OtherMessagesTableViewCell {
                    cell.messageLabel.text = self.firstMessage?.message
                    
                    return cell
                }
            }
        }
        
        else {
            let newIndex = indexPath.row - 1
            
            if self.userID == Int(self.chatMessages[newIndex].fromUserId!) {
                if let cell: MyMessagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? MyMessagesTableViewCell {
                    
                    cell.messageLabel.text = self.chatMessages[newIndex].message
                    
                    return cell
                }
            } else {
                if let cell: OtherMessagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: OthercellId, for: indexPath) as? OtherMessagesTableViewCell {
                    cell.messageLabel.text = self.chatMessages[newIndex].message
                    
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
}

extension ChatRoomViewController: UITextFieldDelegate {
    
}
