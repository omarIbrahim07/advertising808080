//
//  AllAdvertisementsViewController.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

class AllAdvertisementsViewController: BaseViewController {

//    var allCategories = Categories(map: <#Map#>)
    var allCategories : Categories?
//    var allCategories : Categories!
    var noOfCategories : [Category] = []
//    var category : Category?
    var categoryAdvertisments = [AdvertisementDetailsModel]()
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAdvertisementButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureTableView()
        getAllAdvertisements()
        // Do any additional setup after loading the view.
    }
    
    func configureUI() {
        addAdvertisementButton.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2980392157, blue: 0.5882352941, alpha: 1)
        addAdvertisementButton.addCornerRadius(raduis: addAdvertisementButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "AllAdvertisementsTableViewCell", bundle: nil), forCellReuseIdentifier: "AllAdvertisementsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getAllAdvertisements() {
        
        self.startLoading()
        
        CategoriesAPIManager().getAllAdvertisements(basicDictionary: [:], onSuccess: { (categories) in
            self.allCategories = categories
            if let categoriesArr: [Category] = self.allCategories?.categories {
                self.noOfCategories = categoriesArr
                for category in categoriesArr {
                    
                    if let advertisementsInCategory = category.categoryAdvertisements {
                        self.categoryAdvertisments = advertisementsInCategory
                    }
                }
            }
            
            self.tableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    @IBAction func addAdvertisementButton(_ sender: Any) {
        if let addNavigationController = storyboard?.instantiateViewController(withIdentifier: "FirstLevelViewControllerNavigationController") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? FirstLevelViewController {
            rootViewContoller.isEdited = 0
            self.present(addNavigationController, animated: true, completion: nil)
        }
    }
    

}

extension AllAdvertisementsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noOfCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if let cell: AllAdvertisementsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AllAdvertisementsTableViewCell") as? AllAdvertisementsTableViewCell {

                if let header = noOfCategories[indexPath.row].categoryName {
                    cell.headerLabel.text = header
                }
                cell.viewModels = noOfCategories[indexPath.row].categoryAdvertisements ?? []
                cell.delegate = self


                return cell

            }

        
        return UITableViewCell()
    }
    
}


//MARK:- MyAdvertisementTableViewCellDelegate
extension AllAdvertisementsViewController: AllAdvertisementsTableViewCellDelegate {
    
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "AdvertisementDetailsViewController") as? AdvertisementDetailsViewController {
            viewController.advertisementDetails = viewModel
            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
    

}
