//
//  SpecialAdvertisementsTableViewCell.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/13/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import UIKit

protocol AllAdvertisementsTableViewCellDelegate {
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel)
}

class AllAdvertisementsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var parentVC: AllAdvertisementsViewController?
    
    var delegate: AllAdvertisementsTableViewCellDelegate?
    
    var viewModels : [AdvertisementDetailsModel] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionView()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        // Configure the view for the selected state
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "AllAdvertisementsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AllAdvertisementsCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    //MARK:- Delegate Helpers
    func didSelectCollectionViewCell(viewModel: AdvertisementDetailsModel) {
        if let delegateValue = delegate {
            delegateValue.didSelectCollectionViewCell(viewModel: viewModel)
        }
    }
    
}

extension AllAdvertisementsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell: AllAdvertisementsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllAdvertisementsCollectionViewCell", for: indexPath) as? AllAdvertisementsCollectionViewCell {
            
            cell.viewModell = viewModels[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectCollectionViewCell(viewModel: viewModels[indexPath.row])
    }
    
    
}
